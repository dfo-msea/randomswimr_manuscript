# Working title: Quantifying MPA connectivity and movement corridors of groundfish species using a model of random swim

__Main authors:__ John Cristiani, Patrick Thompson  
__Contributors:__ Mairin Deith, Emily Rubidge, ...  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Pacific Biological Station  
__Contact:__      e-mail: john.cristiani@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective

## Summary

## Status

## Contents

## Methods

## Requirements

## Caveats

## Uncertainty

## Acknowledgements

## References


