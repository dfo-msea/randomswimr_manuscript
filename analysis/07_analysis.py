
# Calculate proportion of grid cells with frequency >x% (by ALL and by species)
# Calculate count of connections by cur scaler (do by ALL, then species - present as range)

import pandas as pd
import geopandas as gpd
import pyreadr
import rasterio
from rasterio import features
from shapely.geometry import LineString, Point
import os
import numpy as np

base = r'C:\Users\cristianij\Documents\Projects\randomswimr\randomswimr_manuscript'
out = os.path.join(base, 'analysis/spatial/outputs_20240126/outputs_processed')
gpkg_lines = os.path.join(out, 'conn_lines.gpkg')
gpkg_pts = os.path.join(out, 'src_snk_pts.gpkg')



swims = os.path.join(base, 'analysis/spatial/outputs_20240126/outputs_swims')
species_dist = pd.read_csv(os.path.join(base, 'analysis/spatial/species_movement.csv'),  encoding = "ISO-8859-1")
template_rast = rasterio.open(os.path.join(base, 'analysis/spatial/original_inputs/current_EW.tif'))
gpkg_mpas = os.path.join(base, 'analysis/spatial/mpas/mpas.gpkg')
studyarea = gpd.read_file(gpkg_mpas, layer=f'habcurrents_02_max').geometry

# create polygon mask
# I want cells within the study area to be 0 not -9999
mask = rasterio.features.geometry_mask(
            studyarea.geometry,
            out_shape=template_rast.shape,
            transform=template_rast.transform)

cscalers = [0, 0.1, 0.3]
thresholds = [10, 100, 500, 1000]


# Dataframes output from simulations:
# each row includes to and from id of the ultimate path between MPAs, but each 
# row represents a single cell (patch) that was passed through to complete that 
# path.
# 
# "reps_success" is how many times those two MPAs were connected, and then 
# "count" is of those times, how many times that specific patch was used.
# 
# A patch can be used multiple times between different sets of MPAs (For 
# American Shad, MPA 308 -> 235 and MPA 308 -> 260 both pass through patch 
# 2364).



#################################################################
# ALL

# raster cells used
df_all = pd.DataFrame()

for cs in cscalers:
    cs_str = str(int(cs * 100))
    src = rasterio.open(os.path.join(out, f'paths_ALL_cs{cs_str}.tif'))
    array = src.read()[0]
    # set nodata values to 0
    array = np.where(array<0, 0, array)
    # mask
    array_m = np.where(~mask, array, -999)
    df = pd.DataFrame()
    df['values'] = array_m.ravel()

    # remove masked values
    df = df[df.values != -999]

    total = df.count()
    # percent greater than 0 (so at least used once by any species)
    countg0 = df[df.values > 0].count()
    # greater than 6% (10 out of 150 reps)
    countg6 = df[df.values > 0.066].count()

    df_t = pd.DataFrame({'Current scaler':[cs], 
                         'Percent of raster cells used for at least 1 successful swim':countg0/total*100, 
                         'Percent of raster cells used for at least 10 successful swims':countg6/total*100})
    df_all = pd.concat([df_all, df_t])

df_all = df_all.reset_index()
df_all = df_all.drop(columns=['index'])
df_all.to_csv(os.path.join(out, 'summary_rastercells.csv'))


# connections
df_all = pd.DataFrame()

for cs in cscalers:
    cs_str = str(int(cs * 100))
    lines = gpd.read_file(gpkg_lines, layer=f'conns_ALL_cs{cs_str}')
    lcount = len(lines)
    lmean = lines.conn_strength.mean()
    lmed = lines.conn_strength.median()

    df_t = pd.DataFrame({'Current scaler':[cs], 
                         'Connections count':lcount, 
                         'Connections mean':lmean,
                         'Connections median':lmed})
    df_all = pd.concat([df_all, df_t])

df_all = df_all.reset_index()
df_all = df_all.drop(columns=['index'])
df_all.to_csv(os.path.join(out, 'summary_connections.csv'))
              


###################################
# Individual species results

# rasters
df_all = pd.DataFrame()
for cs in cscalers:
    cs_str = str(int(cs * 100))

    for i, species in species_dist.iterrows():
        sp = species.species
        sp = sp.replace(" ", "").lower()
        mb = species.movement_bin

        tif = os.path.join(os.path.join(out, f'paths_{sp}_cs{cs_str}.tif'))
        if not os.path.exists(tif):
            continue
        src = rasterio.open(tif)
        array = src.read()[0]
        # set nodata values to 0
        array = np.where(array<0, 0, array)
        # mask
        array_m = np.where(~mask, array, -999)
        df = pd.DataFrame()
        df['values'] = array_m.ravel()

        # remove masked values
        df = df[df.values != -999]

        total = df.count()
        # percent greater than 0 (so at least used once)
        countg0 = df[df.values > 0].count()
        # greater than 6% (10 out of 150 reps)
        countg6 = df[df.values > 0.066].count()

        df_t = pd.DataFrame({'Current scaler':[cs],
                             'Species':sp, 
                             'Threshold':mb,
                            'Percent of raster cells used for at least 1 successful swim':countg0/total*100, 
                            'Percent of raster cells used for at least 10 successful swims':countg6/total*100})
        df_all = pd.concat([df_all, df_t])

df_all = df_all.reset_index()
df_all = df_all.drop(columns=['index'])
df_all.to_csv(os.path.join(out, 'summary_rastercells_species.csv'))


# connections
df_all = pd.DataFrame()
for cs in cscalers:
    cs_str = str(int(cs * 100))

    for i, species in species_dist.iterrows():
        sp = species.species
        sp = sp.replace(" ", "").lower()
        mb = species.movement_bin

        try:
            lines = gpd.read_file(gpkg_lines, layer=f'conns_{sp}_cs{cs_str}')
        except Exception:
            continue
        lcount = len(lines)
        lmean = lines.conn_strength.mean()
        lmed = lines.conn_strength.median()

        df_t = pd.DataFrame({'Current scaler':[cs],
                             'Species':sp,
                             'Threshold':mb, 
                            'Connections count':lcount, 
                            'Connections mean':lmean,
                            'Connections median':lmed})
        df_all = pd.concat([df_all, df_t])

df_all = df_all.reset_index()
df_all = df_all.drop(columns=['index'])
df_all.to_csv(os.path.join(out, 'summary_connections_species.csv'))
