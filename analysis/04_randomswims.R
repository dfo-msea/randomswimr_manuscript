
# When running the connectivity simulation, we want to filter our list of
# destination MPAs for each origin MPA, and therefore we need to call
# connectivity_sim for each origin MPA.

# randomswimr functions are called individually (not wrapped in 
# connectivity_sim) because we want to create_seascape() only once for each
# species

# Maxsteps vary by species threshold and are a percentage of the straight line
# distance steps for that threshold distance.


start_time <- Sys.time()
print(start_time)


library(multispeciesconnect)
library(tidyverse)
library(sf)
library(terra)
library(gdistance)


base <- 'C:/Users/cristianij/Documents/Projects/randomswimr/randomswimr_manuscript'
# SDM data
load(file.path(base, 'analysis/spatial/original_inputs/integrated_gf_maps_selected.RData'))
sdms <- sf::st_as_sf(selected_maps,coords = c("X_m", "Y_m"), remove = FALSE,crs = 'EPSG:3005')
sdms <- sdms %>% mutate(est = p_occur)
# Species distance threshold data
species_dist <- read.csv(file.path(base, 'analysis/spatial/species_movement.csv'))
# MPA distances
mpas_distance <- read.csv(file.path(base, 'analysis/spatial/mpas_distance.csv'))
# MPAs spatial data
mpas <- st_read(file.path(base, 'analysis/spatial/mpas/mpas.gpkg'), layer='mpas_05_dissolved')
mpas <- mpas %>% mutate(Name = gID_diss)
# Oceanic currents
current_EW <- system.file("extdata/current_EW.tif", package="multispeciesconnect")
current_NS <- system.file("extdata/current_NS.tif", package="multispeciesconnect")
current_EW <- rast(current_EW)
current_NS <- rast(current_NS)


####################################
####################################


# Simulation parameters
cscalers <- c(0.0, 0.1, 0.3) # change this list to run across different computers
tar_scaler <- 0.0
min_suitable <- 0.1
perc_strln_steps <- 3000
reps <- 150
connect_type = 'start to all ends'

# lengths of objects for status message
cs_len <- length(cscalers)
sp_len <- nrow(species_dist)
mp_len <- nrow(mpas)


####################################
####################################
# for loops nested: current scaler, species, origin mpa


#########################
# cur_scaler
for (row_cs in 1:length(cscalers)){
  cur_scaler <- cscalers[row_cs]
  
  
  ################################
  # species
  for (row_sp in 1:nrow(species_dist)){
    
    # break up across instances:
    #if (row_sp <= 33){next}
    
    sp <- species_dist[row_sp, 'species']
    movebin <- species_dist[row_sp, 'movement_bin']
    
    # calculate number of steps
    threshsteps <- movebin/3
    maxstep <- as.integer(perc_strln_steps / 100 * threshsteps)

    # SDM data subset
    sdm <- sdms %>% filter(species==sp)
    # SDMs are 9 points per 3x3km cell. This is causing the random swim to crash
    # without an error.
    sdm_rast <- terra::rasterize(sdm, current_EW, field='est', fun='mean')
    sdm <- sdm_rast %>% as.points() %>% st_as_sf()
    hab_suit <- sdm %>% 
      mutate(
        species = sp, 
        est = mean,
        X_m = sf::st_coordinates(sdm)[,1],
        Y_m = sf::st_coordinates(sdm)[,2]) %>% 
      dplyr::select(-mean)
    
    # reduce habitat to intersection with current data
    hab_suit_rast <- hab_suit %>% 
      terra::rasterize(current_EW, field='est') %>%
      terra::intersect(current_EW)
    names(hab_suit_rast) <- 'habitat'
    hab_suit <- hab_suit_rast %>%
      terra::as.points() %>% 
      st_as_sf() %>%
      filter(habitat==1) %>%
      st_join(hab_suit, left=FALSE)
    
    # Template raster
    crs_sdm <- st_crs(hab_suit)$epsg
    cellsize <- xres(current_EW)
    template_grid <- sf::st_make_grid(
      st_bbox(terra::ext(hab_suit_rast)),
      square = TRUE, cellsize = cellsize,
      crs = crs_sdm) %>%
      st_sf() %>%
      st_join(hab_suit, join = st_intersects, left=FALSE) %>%
      # Filter the areas not included in the SDM
      filter(habitat==1) %>%
      mutate(mask = 1) %>%
      dplyr::select(mask)
    
    template_raster <- terra::rasterize(template_grid, current_EW, field='mask')

    # Seascape inputs
    st_agr(hab_suit) <- 'constant'
    st_agr(template_grid) <- 'constant'
    patch_coords <- hab_suit %>%
      #st_intersection(template_grid) %>%
      st_drop_geometry() %>%
      dplyr::select(X_m, Y_m) %>%
      unique()
    patch_coords_sf <- sf::st_as_sf(patch_coords, coords = c("X_m", "Y_m"),
                                    remove = FALSE, crs = crs_sdm)
    current_NS_crop <- terra::crop(current_NS, y = template_grid)
    current_EW_crop <- terra::crop(current_EW, y = template_grid)
    names(current_NS_crop) <- "current_NS_crop"
    names(current_EW_crop) <- "current_EW_crop"
    habitat <- hab_suit %>%
      #st_intersection(template_grid) %>%
      dplyr::select(geometry, suit_habitat = "est") %>%
      distinct()
    message("Creating seascape")
    seascape_output <- create_seascape(
      current_NS = current_NS_crop,
      current_EW = current_EW_crop,
      template_grid = template_grid,
      sp_habitat = habitat,
      tcur_scaler = cur_scaler,
      dist_jump = cellsize,
      ignore_checks = TRUE)
    patch_pts <- seascape_output[[1]]
    distances <- seascape_output[[2]]
    move_prob <- seascape_output[[3]]
    rm(seascape_output)
    patch_pts <- st_join(patch_pts, patch_coords_sf)
    # Calculate transition matrix between cells
    coast_raster_raw <- gdistance::transition(
      raster(template_raster), transitionFunction = mean,
      directions = 8
    )
    coast_raster_tr <- gdistance::geoCorrection(
      coast_raster_raw, type = "c")
    
    
    #######################################
    # MPAs
    for (row_mpa in 1:nrow(mpas)) {
      mpa <- mpas[row_mpa, 'gID_diss']$gID_diss
      # destination MPAs
      dest_mpas <- mpas_distance %>% filter(gID_diss_ORIG == mpa & PathCost <= (movebin * 1000))
      dest_mpas <- dest_mpas %>% pull(gID_diss_DEST)
      ds_len <- length(dest_mpas)
      if (ds_len==0){next}
      message(paste(
        "\n",
        "================================\n",
        "Processing:\n",
        "Current scaler: ", row_cs, "of ", cs_len, "\n",
        "Species: ", row_sp, "of ", sp_len, "\n",
        "MPA origin: ", row_mpa, "of ", mp_len, "\n",
        "with # of destination MPAs: ", ds_len, "\n",
        "================================\n"
      ))
      
      start_end <- mpas
      start <- mpa
      end_names <- dest_mpas
      # Create overlapping areas of start/stop patches and the SDM
      target_patches <- patch_pts %>%
        st_join(start_end %>% filter(Name %in% c(start, end_names)),
                join = st_intersects) %>%
        filter(!is.na(Name))
      patch_coords <- data.frame(st_coordinates(patch_pts))
      
      # random swim simulation
      cat(paste0(
        "\nSimulating from ",
        start,
        " to ANY of [",
        paste(end_names, collapse = ","),
        "]\n"
      ))
      start_patches <- patch_pts %>%
        st_join(start_end %>% filter(Name == start),
                join = st_intersects) %>%
        filter(!is.na(Name))
      fromCoords <-
        data.matrix(data.frame(st_coordinates(start_patches)))
      toCoords = data.matrix(patch_coords)
      dist_from_start <- gdistance::costDistance(x = coast_raster_tr,
                                                 # From habitat patches
                                                 fromCoords = fromCoords,
                                                 toCoords = toCoords)
      mindist <- apply(dist_from_start, 2, min)
      patch_pts <- patch_pts %>%
        mutate(dist_from_start = mindist)
      target_end <- patch_pts %>%
        st_join(start_end %>% filter(Name %in% end_names),
                join = st_intersects) %>%
        filter(!is.na(Name)) %>%
        pull(patch)
      target_start <- patch_pts %>%
        st_join(start_end %>% filter(Name == start),
                join = st_intersects) %>%
        filter(!is.na(Name)) %>%
        dplyr::slice_max(suit_habitat) %>%
        filter(suit_habitat > min_suitable) %>%
        pull(patch)
      if (length(target_start) == 0) {
        message("No suitable habitat in starting patch")
        next
      }

      if (tar_scaler!=0.0){
        start_to_end_scaled_matrix <- target_attract(
          tpatch_pts = patch_pts,
          tdistances = distances,
          move_prob = move_prob,
          cell_size = cellsize,
          ttar_scaler = tar_scaler,
          target_distances = "dist_from_start",
          repel = TRUE)
      } else {
        start_to_end_scaled_matrix <- move_prob
      }

      target_patches <- patch_pts %>%
        st_join(start_end %>% filter(Name %in% end_names))
      # Run random swim
      if (length(target_start) > 0) {
        toend_path_unique <- run_random_swim_one_to_all(
          tpatch_pts = patch_pts,
          target_patches = target_patches,
          move_matrix = start_to_end_scaled_matrix,
          tdistances = distances,
          ttarget = target_end,
          tinitial = target_start,
          treps = reps,
          tmaxStep = maxstep,
          start = start,
          end_names = end_names
        )
        if (!is.null(toend_path_unique) && nrow(toend_path_unique) > 0) {
          all_paths <- toend_path_unique %>%
            dplyr::mutate(start = start,
                          cur_scaler = cur_scaler,
                          tar_scaler = tar_scaler)
          # bind all_paths
          if (!exists("all_paths_mpaorig")) {
            all_paths_mpaorig <- all_paths
          } else {
            all_paths_mpaorig <- bind_rows(all_paths_mpaorig, all_paths)
          }
        } else {
          message("No paths generated")
          next
        }
      }
      
    } # for-loop mpas
    
    # set up filename
    sp_f <- tolower(gsub(" ", "", sp))
    cs_f <- as.integer(cur_scaler * 100)
    out_name <- paste("paths_", sp_f, "_cs", cs_f, ".rds", sep='')
    
    # write df to .rds.
    if (exists("all_paths_mpaorig")) {
      all_sp <- all_paths_mpaorig %>%
        dplyr::mutate(species = sp) %>%
        st_drop_geometry() %>%
        data.frame() # need to convert to basic df if I want to read with pyreader
      saveRDS(all_sp, file.path(base, 'analysis/spatial/outputs_raw', out_name))
      # Account for if no paths written, but it still exists for previous species
      rm(all_paths_mpaorig, all_paths, all_sp)
    }
    
    
  } # for-loop species

} # for-loop cur_scaler


# Output data frames:
# each row includes to and from id of the connected MPAs, but eachrow represents
# a single cell (patch) that was passed through to complete that path.
# 
# "reps_success" is how many times those two MPAs were connected, and then 
# "count" is of those times, how many times that specific patch was used.


end <- Sys.time()
print(end)
