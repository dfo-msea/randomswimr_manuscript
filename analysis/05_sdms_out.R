
# output all sdms to tif for visual comparison with results


library(tidyverse)
library(sf)
library(terra)


base <- 'C:/Users/cristianij/Documents/Projects/randomswimr/randomswimr_manuscript'
# SDM data
load(file.path(base, 'analysis/spatial/original_inputs/integrated_gf_maps_selected.RData'))
sdms <- sf::st_as_sf(selected_maps,coords = c("X_m", "Y_m"), remove = FALSE,crs = 'EPSG:3005')
sdms <- sdms %>% mutate(est = p_occur)
# Species distance threshold data
species_dist <- read.csv(file.path(base, 'analysis/spatial/species_movement.csv'))
# Oceanic currents
current_EW <- system.file("extdata/current_EW.tif", package="multispeciesconnect")
current_EW <- rast(current_EW)

out <- file.path(base, 'analysis/spatial/original_inputs/sdms_rast')


################################
# species
for (row_sp in 1:nrow(species_dist)) {
  sp <- species_dist[row_sp, 'species']
  sdm <- sdms %>% filter(species == sp)
  # SDMs are 9 points per 3x3km cell. This is causing the random swim to crash
  # without an error.
  sdm_rast <-
    terra::rasterize(sdm, current_EW, field = 'est', fun = 'mean')
  
  sp_f <- tolower(gsub(" ", "", sp))
  out_name <-
    file.path(out, (paste("sdm_", sp_f, ".tif", sep = '')))
  
  writeRaster(sdm_rast, out_name)
  
}    
