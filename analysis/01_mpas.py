
# Limit MPA dataset to intersection of oceanographic and habitat datasets
# Remove areas where oceanographic/habitat pixels are isolated
# Dissolve MPAs

# This was done before I realized there was already a mask dataset for the oceanographic data.

# To note:
# There were issues with the mask used for clipping currents in the generate currents dataset.
# This was revealed by the distance analysis. 
# To the west of Haida Gwaii we end up with only one diagonal path between cells at one 
# point. The distance analysis cannot go through this (even if the lines get made diagonally). I 
# ended up editing the mask in just this area to create an extra cell.
# BCCM_NSB_OceanExtent_JCedit.shp


import pandas as pd
import geopandas as gpd
import rasterio
from rasterio import features
import pyreadr
import os
from osgeo import gdal
import shapely


base = r'C:\Users\cristianij\Documents\Projects\randomswimr'
gpkg = os.path.join(base, 'randomswimr_manuscript/analysis/spatial/mpas/mpas.gpkg')
inputs = os.path.join(base, 'randomswimr_manuscript/analysis/spatial/original_inputs')
currentRas = os.path.join(base, 'package_development/multispeciesconnectivityr/inst/extdata/current_EW.tif')






# Copy MPA feature class, this also creates the geopackage
# Proposed network feature class. This includes existing MPAs as well as Category 1 (2025) MPAs and 
# Category 2 (2030) MPAs.
gdb = os.path.join(inputs, 'Spatial_Q.gdb')
mpas = gpd.read_file(gdb, driver='fileGDB', layer='Proposed_MPA_Network_NSB_Jan2023_PUBLIC')
mpas.to_file(gpkg, layer='mpas_01_original', driver='GPKG')

# Read in .RData habitat data, groupby, take max, out to points
hab_rdata = os.path.join(inputs, 'integrated_gf_maps_selected.RData')
hab = pyreadr.read_r(hab_rdata)
habdf = hab['selected_maps']
habdf_group = habdf.groupby(['X_m', 'Y_m']).agg(
    p_occur = ('p_occur', 'max')
).reset_index()
habgdf = gpd.GeoDataFrame(habdf_group, geometry=gpd.points_from_xy(habdf_group.X_m, habdf_group.Y_m), crs='EPSG:3005')
habgdf.to_file(gpkg, layer='habitat_01_pts', driver='GPKG')


# rasterize habitat data
orast = rasterio.open(currentRas)
# there doesn't seem to be a way to do "max" value like you can with arcgis, so its important to 
# reduce the dataframe as I do above with groupby
geom_value = ((geom,value) for geom, value in zip(habgdf.geometry, habgdf.p_occur))
rasterized = features.rasterize(geom_value,
                                out_shape = orast.shape,
                                fill = -9999,
                                out = None,
                                transform = orast.transform,
                                all_touched = True,
                                #default_value = 1,
                                dtype = None)
with rasterio.open(
        os.path.join(base, "randomswimr_manuscript/analysis/spatial/mpas/habitat_02_rast.tif"), "w",
        driver = "GTiff",
        crs = orast.crs,
        transform = orast.transform,
        dtype = rasterio.float32,
        count = 1,
        width = orast.width,
        nodata = -9999,
        height = orast.height) as dst:
    dst.write(rasterized, indexes = 1)
# It looks like you can't write directly to a geopackage. This doesn't seem right, but not going to
# worrry about it now. Do it with GDAL.
#ds = gdal.Open(os.path.join(base, "analysis/spatial/mpas/habitat_02_rast.tif"))
#ds = gdal.Translate(gpkg, ds, options="-of GPKG -co APPEND_SUBDATASET=YES -co RASTER_TABLE=habitat_02_rast") # this writes out as "mpas", but if you run it again to a different name, the correct name then appears. However, if you delete the second one then it diverts back to being just "mpas". Somethings wrong. And then you can't delete the mpas one either.
#ds=None


# Oceanographic data - convert to polys, dissolve
orast = rasterio.open(currentRas)
orast_1 = orast.read(1) # in this case you need to specify the band to get the dtype
polygonize = features.shapes(orast_1, transform=orast.transform)
polygons = []
values = []
for shape, value in polygonize:
    polygons.append(shapely.geometry.shape(shape))
    values.append(value)
gdf = gpd.GeoDataFrame({'u':values, 'geometry':polygons}, crs="EPSG:3005")
gdf = gdf[gdf.u.notna()]
gdf = gdf.dissolve()
gdf = gdf.explode(ignore_index=True)
gdf['u'] = 1
gdf.to_file(gpkg, layer='current_EW_polys', driver='GPKG')


# Convert habitat raster to polys, dissolve
hrast = rasterio.open(os.path.join(base, 'randomswimr_manuscript/analysis/spatial/mpas/habitat_02_rast.tif'))
hrast_1 = hrast.read(1) # in this case you need to specify the band to get the dtype
polygonize = features.shapes(hrast_1, transform=hrast.transform)
polygons = []
values = []
for shape, value in polygonize:
    polygons.append(shapely.geometry.shape(shape))
    values.append(value)
gdf = gpd.GeoDataFrame({'p_occur':values, 'geometry':polygons}, crs="EPSG:3005")
gdf = gdf[gdf.p_occur>0]
gdf = gdf.dissolve()
gdf = gdf.explode(ignore_index=True)
gdf['p_occur'] = 1
gdf.to_file(gpkg, layer='habitat_03_polys', driver='GPKG')


# intersect oceanographic and habitat polys (this is going to happen in the package anyways, but do
# it here so that we know which MPAs will be eliminated)
hdf = gpd.read_file(gpkg, layer='habitat_03_polys')
odf = gpd.read_file(gpkg, layer='current_EW_polys')
df_int = gpd.overlay(hdf, odf, how='intersection', keep_geom_type=True)
# overlay results in both multipolygon and polygon geometry types. It can output this format to
# geopackage, but it is annoying with how it is viewed in qgis. So make them all multipolygon.
from shapely.geometry.polygon import Polygon
from shapely.geometry.multipolygon import MultiPolygon
df_int["geometry"] = [MultiPolygon([feature]) if isinstance(feature, Polygon) else feature for \
                      feature in df_int["geometry"]]
df_int.to_file(gpkg, layer='habcurrents_01_intersect', driver='GPKG')

# # It looks like not all diagonal pieces are included as part of the biggest poly (some are, others 
# # aren't). Perhaps this was something with the multiple geometry types thing above.
# # (note, pandas doesn't merge if just the edges touch, so cant just dissolve and explode)
# # Instead, buffer, union, negative buffer
# df_diss = df_int.buffer(0.1).unary_union.buffer(-0.1)
# df_diss = gpd.GeoDataFrame(geometry=gpd.GeoSeries(df_diss), crs="EPSG:3005")
# df_diss = df_diss.explode(ignore_index=True)
# df_diss['uid'] = df_diss.index
# df_diss.to_file(gpkg, layer='habcurrents_02_diss', driver='GPKG')

# delete any isolated pieces
# get area, remove all but the biggest piece
df_int['area'] = df_int.geometry.area
df_max = df_int[df_int.area == df_int.area.max()]
df_max.to_file(gpkg, layer='habcurrents_02_max', driver='GPKG')



# note which MPAs don't overlap intersect of oceanographic model and habitat data extent
dfho = gpd.read_file(gpkg, layer='habcurrents_02_max')
dfmpas = gpd.read_file(gpkg, layer='mpas_01_original')
dfmpas_sj = dfmpas.sjoin(dfho, how='left').reset_index()
dfmpas_sj.loc[~dfmpas_sj['p_occur'].isna(), 'model_overlap'] = 1
dfmpas_sj = dfmpas_sj.drop(columns=['p_occur', 'area', 'index_right', 'area'])
dfmpas_sj.to_file(gpkg, layer='mpas_02_modeloverlap', drive='GPKG')
# output a version with MPAs removed
df_include = dfmpas_sj[dfmpas_sj.model_overlap == 1]
df_include.to_file(gpkg, layer='mpas_03_toinclude', driver='GPKG')


# I don't want to then clip. I'll get a lot of slivers and discontinuous pieces. The randomswimr 
# package will only initiate in overlapping areas anyways. It will be helpful to see the full MPAs 
# here.


# DISSOLVING

# First, try a basic dissolve and explode and see what I get.
# mpas = gpd.read_file(gpkg, layer='mpas_03_toinclude')
# mpas_diss = mpas.dissolve()
# mpas_exp = mpas_diss.explode(ignore_index=True)
# mpas_exp.to_file(gpkg, layer='mpas_04_dissexp', drive='GPKG')
# We end up making individual polys out of very very small polys that should be considered together.
# Try union, make new field, dissolve
# mpas = gpd.read_file(gpkg, layer='mpas_03_toinclude')
# mpa_u = mpas.unary_union()  # getting a geometry error
# mpa_u = mpas.buffer(0.1).unary_union() # get a geometry error


# Fuck, I don't see any other way than to do a lot of this grouping manually if I want it done 
# right. Essentially, some things shouldn't be exploded (MPA with a lot of small islands), and some 
# things shouldn't be dissolved (all of Haida Gwaii, nearshore/offshore areas). We don't have the 
# right kind of mpa and mpa-part IDs to be able to do this correctly, so I will need to set this up
# manually.

# identify which ones to explode first (e.g., sponge reefs, southern haida gwaii). These are ones
# that we want to treat separately because the pieces are very far apart.
# In QGIS, right click on field and zoom to feature. If you have the esri oceans layer on underneath
# then it is pretty easy to judge the scale.
mpas = gpd.read_file(gpkg, layer='mpas_03_toinclude')
to_explode_first = ['S2_CC_830', 'S2_CC_831', 'S2_HG_840', 'S2_HG_841', 'S2_NC_833', 'S2_NC_834', 'S2_NVI_673']
# select these out
mpas_exp = mpas[mpas.UID.isin(to_explode_first)]
# explode them
mpas_exp = mpas_exp.explode(ignore_index=True)
# with a df of these selected out, append them back in
mpas_noexp = mpas[~mpas.UID.isin(to_explode_first)]
mpas_all = pd.concat([mpas_exp, mpas_noexp], ignore_index=True)
from shapely.geometry.polygon import Polygon
from shapely.geometry.multipolygon import MultiPolygon
mpas_all["geometry"] = [MultiPolygon([feature]) if isinstance(feature, Polygon) else feature for \
                      feature in mpas_all["geometry"]]
# create a new field of unique IDs that I will use for dissolving
mpas_all['gID'] = mpas_all.index + 1
mpas_all.to_file(gpkg, layer='mpas_04_exp', driver='GPKG')


# MANUALLY...
# go through each feature, identify if it should be dissolved with another piece,
# if so, assign it the new unique ID of the first one I come across for each group.
# If not, then I don't need to add it to the list.
# Ideally I am not splitting any individual polys. This will make assigning back original IDs much
# easier.
# Also, note which ones should be removed (e.g. a large MPA in an inlet that barely overlaps with
# the grid.)
#mpas_all.to_file('C:/Users/cristianij/Desktop/TEMPshp.shp')
to_dissolve = { # key is the grouping ID. Grouping is based on exposure, offshore/nearshore, and bathymetry.
    141:[144,145,146],
    51:[52,90,126,127,132,133],
    1:[3],
    140:[150],
    42:[43,44,45],
    46:[139,149],
    135:[136,137,138],
    53:[56,83],
    57:[58,60],
    62:[63,64,65,66],
    69:[76,82],
    73:[74],
    75:[81],
    2:[4,5,6,8,86,162],
    7:[9,159],
    152:[154],
    70:[71,72],
    78:[158],
    118:[119],
    87:[97,105,115], #HG North
    88:[91,110,111,113,114,117], #HG Northwest
    93:[94,95,98,100,102,103,104,107,116], # HG West
    15:[23,26,29,37,39,108,155], #HG Southwest
    10:[10,11,12,13,14,16,17,18,19,20,21,22,24,25,27,28,30,31,32,33,34,35,36,38,40,41,89,101,112], # HG Southeast
    47:[48,49,50,120,121,122,123,124,128,129,130,131,134] # Scott Islands inner
}
to_remove = [143,148,59,55,106]


# remove MPAs in list
mpas_all = gpd.read_file(gpkg, layer='mpas_04_exp')
mpas_all = mpas_all[~mpas_all.gID.isin(to_remove)]

# Create a new dissolve ID field to hold new IDs
mpas_all['gID_diss'] = mpas_all.gID
# Go through each key, for the gIDs in each key, make the dissolve ID equal to
# the key.
for key in to_dissolve:
    mpas_all.loc[mpas_all['gID'].isin(to_dissolve[key]), 'gID_diss'] = key

# then I can finally dissolve
mpas_diss = mpas_all.dissolve(by='gID_diss', as_index=False)
mpas_diss = mpas_diss[['gID_diss', 'geometry']]
from shapely.geometry.polygon import Polygon
from shapely.geometry.multipolygon import MultiPolygon
mpas_diss["geometry"] = [MultiPolygon([feature]) if isinstance(feature, Polygon) else feature for \
                      feature in mpas_diss["geometry"]]
mpas_diss.to_file(gpkg, layer='mpas_05_dissolved', driver='GPKG')

# arcgis has a hard time reading this final version, so convert here
# this version will be used for the distance analysis
mpas_diss = gpd.read_file(gpkg, layer='mpas_05_dissolved')
out = os.path.join(base, 'analysis/spatial/mpas/mpas_dissolved.shp')
mpas_diss.to_file(out)

# VISUALLY CHECK


# So to connect the dissolved polygons back to their original piece, I can use the gID_dissolve
# field to the gID field, which is then associated with the UID of the original dataset.
# I did not edit any original gemoetries, so they should all connect back without duplicates.



###############################################################
###############################################################


# feature count for manuscript
gdb = os.path.join(inputs, 'Spatial_Q.gdb')
mpas_march = gpd.read_file(gdb, driver='fileGDB', layer='Q1_FULL_March2023')
mdiss = mpas_march.dissolve(['Common_site_name_Site_Profile', 'Category_Simple'], aggfunc='first', as_index=False)
type = mdiss.Category_Simple.unique()
for t in type:
    print(t)
    count = len(mdiss[mdiss.Category_Simple==t])
    print(count)
# Existing MPA/RCA - 'as-is, where-is'
# 77
# Category 1
# 43
# Category 2
# 8


# mask
mask = os.path.join(base, 'package_development/masks/BCCM_NSB_OceanExtent_JCedit.shp')
mask = gpd.read_file(mask)
mdsj = gpd.sjoin(mdiss, mask).reset_index()
# further reduced by intersection of habitat and current data
dfho = gpd.read_file(gpkg, layer='habcurrents_02_max')
mdsj = mdsj.drop(columns=['index_right'])
mpi = gpd.sjoin(mdsj, dfho)
type = mpi.Category_Simple.unique()
for t in type:
    print(t)
    count = len(mpi[mpi.Category_Simple==t])
    print(count)
# Category 1
# 23
# Existing MPA/RCA - 'as-is, where-is'
# 15
# Category 2
# 6

# Compare the above numbers to the total number of polys after combining or splitting
# disparate polys:
mpas_diss = gpd.read_file(gpkg, layer='mpas_05_dissolved')
print(len(mpas_diss))
# 47

