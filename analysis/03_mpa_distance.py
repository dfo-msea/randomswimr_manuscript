
# Calculate overwater distance between MPAs.

# Doing distance from edges instead of centroids. Some MPAs are large, and the shortest path the 
# centroid would differ significantly from the edge. I will do just a few points along the edge. 
# This isn't perfect, but it is better than just centers.

# use default arcpy environment. I'm not going to try to recreate this with opensource.
# gdistance method in R is a bit less verbose but doesn't output lines, which are nice to have.

# This distance analysis revealed issues with the mask used for clipping currents. To the west of
# Haida Gwaii we end up with only one diagonal path between cells at one point. The distance
# analysis can not go through this (even if the lines get made diagonally). I ended up editing the
# mask in just this area to create an extra cell.

import arcpy
import os
import pandas as pd
from arcgis.features import GeoAccessor, GeoSeriesAccessor
from arcpy.sa import *
arcpy.CheckOutExtension("Spatial")

base = r'C:/Users/cristianij/Documents/Projects/randomswimr'
gdb = os.path.join(base, 'randomswimr_manuscript/analysis/spatial/mpas/mpas_distance.gdb')
currentRas = os.path.join(base, 'package_development/multispeciesconnectivityr/inst/extdata/current_EW.tif')
arcpy.env.workspace = gdb


# convert shp to gdb
arcpy.CopyFeatures_management(os.path.join(base, 'randomswimr_manuscript/analysis/spatial/mpas/mpas_dissolved.shp'), 'mpas')


# Create points along edges of MPAs. Ideally, I can get a few evenly spaced so that a shortest path
# can be roughly determined from multiple incoming angles.
arcpy.EliminatePolygonPart_management('mpas', 'mpas_01_filled', 'AREA', '400000000')
arcpy.PolygonToLine_management('mpas_01_filled', 'mpas_02_lines', 'IGNORE_NEIGHBORS')
arcpy.AddField_management('mpas_02_lines', 'min_distance', 'FLOAT')
with arcpy.da.UpdateCursor('mpas_02_lines', ['min_distance', 'Shape_Length']) as cursor:
    for row in cursor:
        row[0] = row[1] / 5
        cursor.updateRow(row)
arcpy.CreateRandomPoints_management(gdb, 'mpas_03_points', 'mpas_02_lines', '', 4, 'min_distance')
arcpy.CopyFeatures_management('mpas_03_points', 'mpas_04_ptsmove')

# This kinda works. At this point it will now just be easiest to go in and manually move points.
# For each poly, arrange them in the best position to accept paths from multiple angles. Also,
# delete extra points that aren't needed (e.g., an MPA at the end of an inlet that just needs one
# point on the edge). Do this with the model grid in the background.
# !!MAKE sure that no two points share the same cell.

# After manual edits, create unique point ID
arcpy.AddField_management('mpas_04_ptsmove', 'pID', 'SHORT')
with arcpy.da.UpdateCursor('mpas_04_ptsmove', ['pID', 'OBJECTID']) as cursor:
    for row in cursor:
        row[0] = row[1]
        cursor.updateRow(row)

# Convert ocean model data to cost raster
inRast = Raster(currentRas)
costRas = Con(inRast, 1, '', "Value<100")
costRas.save('costRas')

# Distance analysis
with arcpy.da.SearchCursor('mpas_04_ptsmove', ['CID', 'pID']) as cursor:
    for row in cursor:
        pID = row[1]
        print('processing pID {}'.format(pID))
        centroid_sel = arcpy.SelectLayerByAttribute_management('mpas_04_ptsmove', 'NEW_SELECTION', 'pID={}'.format(pID))

        arcpy.env.snapRaster = currentRas
        arcpy.env.cellSize = currentRas

        # Euclidean distance with barriers
        # No need to specify barriers if cost raster is null in those areas
        outEucDistance = DistanceAccumulation(
            in_source_data = centroid_sel,
            in_cost_raster = 'costRas',
            out_back_direction_raster = 'eucbackdirect')
        
        # Lines
        outlines = f'euc_lines_{pID}'
        OptimalPathAsLine('mpas_04_ptsmove', outEucDistance, 'eucbackdirect', outlines, 'pID')

        # Add origin id and MPA as attribute
        arcpy.AddField_management(outlines, 'Source_pID', 'SHORT')
        arcpy.CalculateField_management(outlines, 'Source_pID', pID)
        arcpy.AddField_management(outlines, 'Source_MPA', 'SHORT')
        arcpy.CalculateField_management(outlines, 'Source_MPA', row[0])        

        arcpy.Delete_management([centroid_sel, 'eucbackdirect', outEucDistance])


# merge
fcs = arcpy.ListFeatureClasses('euc_lines_*')
arcpy.Merge_management(fcs, 'euc_lines_ALL')

# Check lines
# As happened last time, some of the line geometries get screwed up. Manually create a field to
# measure the difference between PathCost and Shape_Length. Not sure why this error persists. It
# looks like it when the line needs to get around a small hole in the grid.
# There's a clear jump in the difference associated with the error (from about 7000 to 30000). 7k is
# associated with the longest line, so a difference of 7k over 500k is not that big of a deal.
# For now, just use PathCost as my distance. It doesn't differ much from ShapeLength.


# join to get source and destination MPAs as fields
# The series of unique IDs got a bit complicated with each iteration of MPAs:
# gID -> gID_diss -> ORIG_FID -> CID
lines = pd.DataFrame.spatial.from_featureclass('euc_lines_ALL')
# join on pID and destID to get mpa_id (CID)
mpas_pts = pd.DataFrame.spatial.from_featureclass('mpas_04_ptsmove')
lines = lines.merge(mpas_pts, left_on='DestID', right_on='pID')
lines['dest_MPA_CID'] = lines.CID
lines = lines[['PathCost', 'Source_MPA', 'dest_MPA_CID']]
# join to mpas_01_filled on ORIG_FID and dest_MPA_CID, create field for gID_diss_DEST
mpas_01 = pd.DataFrame.spatial.from_featureclass('mpas_01_filled')
lines = lines.merge(mpas_01, left_on='dest_MPA_CID', right_on='ORIG_FID')
lines['gID_diss_DEST'] = lines.gID_diss
lines = lines[['PathCost', 'Source_MPA', 'gID_diss_DEST']]
# join to 01_filled on ORIG_FID and source_MPA, create field for gID_diss_ORIG
lines = lines.merge(mpas_01, left_on='Source_MPA', right_on='ORIG_FID')
lines['gID_diss_ORIG'] = lines.gID_diss
lines = lines[['PathCost', 'gID_diss_ORIG', 'gID_diss_DEST']]

# remove self connections
lines = lines[lines.gID_diss_ORIG != lines.gID_diss_DEST]
# for each series of MPA to MPA, retain just the shortest path
lines_min = lines.groupby(['gID_diss_ORIG', 'gID_diss_DEST'])['PathCost'].min().reset_index()

# output table with distance from each MPA to every other MPA
lines_min.to_csv(os.path.join(base, 'randomswimr_manuscript/analysis/spatial/mpas_distance.csv'))

arcpy.Delete_management(['costRas'])
fcs_lines = arcpy.ListFeatureClasses('euc_lines_*')
fcs_lines_ex = fcs_lines[:-1]
arcpy.Delete_management(fcs_lines_ex)
