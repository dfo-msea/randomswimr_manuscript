

# Compare number of MPAs connected

# 2 species
# every threshold tested for each species
# 1 MPA: 75
# For each threshold, we tested six maxstep values caluclated as a percentage of the straightline
# distnace of steps for the threshold.

# We want to vary maxsteps by threshold, so a use a percentage of straighline distance steps as
# opposed to the same steps for each threshold (e.g., doesn't make sense to run 10000 steps for a 
# 10km movement threshold).


import pandas as pd
import geopandas as gpd
import pyreadr
import rasterio
from rasterio import features
import os
import seaborn as sns
import matplotlib.pyplot as plt

base = r'C:\Users\cristianij\Documents\Projects\randomswimr\randomswimr_manuscript'
swims = os.path.join(base, 'analysis/spatial/outputs_20240110_sens_steps_th/outputs_swims')
template_rast = rasterio.open(os.path.join(base, 'analysis/spatial/original_inputs/current_EW.tif'))
out = os.path.join(base, 'analysis/spatial/outputs_20240110_sens_steps_th/outputs_processed')
species_dist = pd.read_csv(os.path.join(base, 'analysis/spatial/species_movement.csv'),  encoding = "ISO-8859-1")
mpas_distance = pd.read_csv(os.path.join(base, 'analysis/spatial/mpas_distance.csv'))


perc_sl = [100, 1000, 2000, 3000, 4000, 5000] # percent of straightline distance steps
thresholds = [10,100,500,1000]
species_mpas = {'Arrowtooth Flounder': [75],
                'Butter Sole': [75]}



###################################
# Get list of connections and count
# Calculate similarity of lists between steps

conns = pd.DataFrame()

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()

    for th in thresholds:
        th = str(th)

        for ms in perc_sl:
            ms = str(ms)

            rds = os.path.join(swims, f'paths_{sp}_cs25_thresh{th}_perstep{ms}.rds')
            if not os.path.exists(rds):
                continue
            df = pyreadr.read_r(rds)
            df = df[None]
            
            df['conn_strength'] = df.reps_success / df.reps

            df = df.groupby(['start', 'target']).agg(
                conn_strength = ('conn_strength', 'mean')
            ).reset_index()

            df['steps_pc'] = int(ms)
            df['species'] = sp
            df['threshold'] = int(th)

            conns = pd.concat([conns, df], ignore_index=True)



# Calculate similarity to a list of all MPAs within range
similarity = pd.DataFrame()

for spe in species_mpas:

    sp = spe.replace(" ", "").lower()

    for th in thresholds:

        # get list of all possible destination MPAs
        dmpas = mpas_distance[(mpas_distance.gID_diss_ORIG.isin(species_mpas[spe])) & (mpas_distance.PathCost <= th*1000)]
        dmpas = dmpas.gID_diss_DEST.tolist()

        for ms in perc_sl:
            targs = conns[(conns.steps_pc == ms) & (conns.species == sp) & (conns.threshold == th)].target.unique().tolist()

            if len(targs)>0:
                # common elements
                com = set(dmpas).intersection(set(targs))
                num_com = len(com)
                # Total unique elements in both lists
                tot = set(dmpas).union(set(targs))
                num_tot = len(tot)

                # % similarity
                perc_sim = (num_com / num_tot) * 100

                df = pd.DataFrame({'Percent of straightline equivalent steps':[ms],
                                'Percent similar to all possible connections':[perc_sim], 
                                'species':[sp],
                                'threshold':[th]})
                similarity = pd.concat([similarity, df], ignore_index=True)
            else:
                df = pd.DataFrame({'Percent of straightline equivalent steps':[ms],
                                'Percent similar to all possible connections':0, 
                                'species':[sp],
                                'threshold':[th]})
                similarity = pd.concat([similarity, df], ignore_index=True)                

similarity['species_threshold'] = similarity.species + '_' + (similarity.threshold).astype(str)


# p = sns.lineplot(data=similarity, 
#                  x='percent_of_straightline_steps', y='percent_sim_to_all', 
#                  hue='species', units='species_threshold', estimator=None, marker='o')
# p.figure.savefig(os.path.join(out, f'conns_similarityAll_slsteps.png'))

palette = sns.color_palette("hls", 4)
sns.set_context("paper")
t = sns.lineplot(data=similarity, 
                 x='Percent of straightline equivalent steps', y='Percent similar to all possible connections', 
                 hue='threshold', units='species_threshold', style='species', estimator=None, marker='o',palette=palette)
t.figure.savefig(os.path.join(out, f'conns_similarityAll_slsteps_h.png'))

# Interpretation:
# We may want to set maxsteps to vary by species. We can see that the y axis saturates at about
# 3000+%. This gives us ONE number to refer to.
# This would be:
# ~10,000 steps for 1000km threshold
# ~8,333 steps for 500km threshold
# ~1,666 steps for 100km threshold
# ~166 steps for 10km threshold
