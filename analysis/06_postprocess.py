
# Calculate proportion rasters, connection lines, and source/sink values for each species and overall

import pandas as pd
import geopandas as gpd
import pyreadr
import rasterio
from rasterio import features
from shapely.geometry import LineString, Point
import os

base = r'C:\Users\cristianij\Documents\Projects\randomswimr\randomswimr_manuscript'
swims = os.path.join(base, 'analysis/spatial/outputs_20240126/outputs_swims')
species_dist = pd.read_csv(os.path.join(base, 'analysis/spatial/species_movement.csv'),  encoding = "ISO-8859-1")
template_rast = rasterio.open(os.path.join(base, 'analysis/spatial/original_inputs/current_EW.tif'))
gpkg_mpas = os.path.join(base, 'analysis/spatial/mpas/mpas.gpkg')
out = os.path.join(base, 'analysis/spatial/outputs_20240126/outputs_processed')
gpkg_lines = os.path.join(out, 'conn_lines.gpkg')
gpkg_pts = os.path.join(out, 'src_snk_pts.gpkg')

cscalers = [0, 0.1, 0.3]
thresholds = [10, 100, 500, 1000]


# Dataframes output from simulations:
# each row includes to and from id of the ultimate path between MPAs, but each 
# row represents a single cell (patch) that was passed through to complete that 
# path.
# 
# "reps_success" is how many times those two MPAs were connected, and then 
# "count" is of those times, how many times that specific patch was used.
# 
# A patch can be used multiple times between different sets of MPAs (For 
# American Shad, MPA 308 -> 235 and MPA 308 -> 260 both pass through patch 
# 2364).



###################################
# Individual species results


### Corridors:
# Raster value = count/reps

for cs in cscalers:
    cs = str(int(cs * 100))
    print(cs)

    for i, species in species_dist.iterrows():
        sp = species.species
        sp = sp.replace(" ", "").lower()
        print(sp)
        rds = os.path.join(swims, f'paths_{sp}_cs{cs}.rds')
        if not os.path.exists(rds):
            continue
        df = pyreadr.read_r(rds)
        df = df[None]

        df['prop_walks_success'] = df['count'] / df.reps
        df = df.groupby('patch').agg(
            X_m = ('X_m', 'first'),
            Y_m = ('Y_m', 'first'),
            prop_walks_success_avg = ('prop_walks_success', 'mean')
        ).reset_index()
        gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.X_m, df.Y_m), crs='EPSG:3005')

        geom_value = ((geom,value) for geom, value in zip(gdf.geometry, gdf.prop_walks_success_avg))
        rasterized = features.rasterize(geom_value,
                                        out_shape = template_rast.shape,
                                        fill = -9999,
                                        out = None,
                                        transform = template_rast.transform,
                                        all_touched = True,
                                        dtype = None)
        with rasterio.open(
                os.path.join(base, out, f'paths_{sp}_cs{cs}.tif'), "w",
                driver = "GTiff",
                crs = template_rast.crs,
                transform = template_rast.transform,
                dtype = rasterio.float32,
                count = 1,
                width = template_rast.width,
                nodata = -9999,
                height = template_rast.height) as dst:
            dst.write(rasterized, indexes = 1)

    

### Pairwise connection strength
# connection strength: reps_success / reps
            
# get MPA centroids
mpas = gpd.read_file(gpkg_mpas, layer='mpas_05_dissolved')
mpas['cent_x'] = mpas.centroid.x
mpas['cent_y'] = mpas.centroid.y

for cs in cscalers:
    cs = str(int(cs * 100))
    print(cs)

    for i, species in species_dist.iterrows():
        sp = species.species
        sp = sp.replace(" ", "").lower()
        print(sp)
        rds = os.path.join(swims, f'paths_{sp}_cs{cs}.rds')
        if not os.path.exists(rds):
            continue
        df = pyreadr.read_r(rds)
        df = df[None]

        df['conn_strength'] = df.reps_success / df.reps
        df = df.groupby(['target', 'start']).agg(
            conn_strength = ('conn_strength', 'mean') # they should all be the same value per group
        ).reset_index()

        # join centroid coordinates
        df = df.merge(mpas, left_on='start', right_on='gID_diss')
        df['fr_pt'] = [Point(xy) for xy in zip(df.cent_x, df.cent_y)]
        df = df.drop(columns=['geometry', 'gID_diss', 'cent_x', 'cent_y'])
        df = df.merge(mpas, left_on='target', right_on='gID_diss')
        df['to_pt'] = [Point(xy) for xy in zip(df.cent_x, df.cent_y)]
        df = df.drop(columns=['geometry', 'gID_diss', 'cent_x', 'cent_y'])

        # create lines between centroids
        df['line'] = df.apply(lambda row: LineString([row['fr_pt'], row['to_pt']]), axis=1)
        gdf = gpd.GeoDataFrame(df, geometry='line')
        gdf = gdf.drop(columns=['fr_pt', 'to_pt'])

        # out to geopackage
        gdf.to_file(gpkg_lines, layer=f'conns_{sp}_cs{cs}', drive='GPKG')



#### Source and sink potential:
# Node in/out degree: count of connections and,
# Node in/out degree weighted: sum of connection strengths
        
# get MPA centroids
mpas = gpd.read_file(gpkg_mpas, layer='mpas_05_dissolved')
mpas['centroid'] = mpas.centroid
mpas = mpas.drop(columns=['geometry'])

for cs in cscalers:
    cs = str(int(cs * 100))
    print(cs)

    for i, species in species_dist.iterrows():
        sp = species.species
        sp = sp.replace(" ", "").lower()
        print(sp)
        rds = os.path.join(swims, f'paths_{sp}_cs{cs}.rds')
        if not os.path.exists(rds):
            continue
        df = pyreadr.read_r(rds)
        df = df[None]

        df['conn_strength'] = df.reps_success / df.reps
        df = df.groupby(['target', 'start']).agg(
            conn_strength = ('conn_strength', 'mean')
        ).reset_index()
        df['i_count'] = 1

        df_out = df.groupby('start').agg(
            source_degree_weighted = ('conn_strength', 'sum'),
            source_degree =  ('i_count', 'sum')).reset_index()
        
        df_in = df.groupby('target').agg(
            sink_degree_weighted = ('conn_strength', 'sum'),
            sink_degree =  ('i_count', 'sum')).reset_index()
        
        mpas_ss = mpas.merge(df_out, left_on='gID_diss', right_on='start', how='left')
        mpas_ss = mpas_ss.merge(df_in, left_on='gID_diss', right_on='target', how='left')

        gdf = gpd.GeoDataFrame(mpas_ss, geometry='centroid')
        gdf = gdf.drop(columns=['start', 'target'])
        gdf.to_file(gpkg_pts, layer=f's_{sp}_cs{cs}', drive='GPKG')




#######################################################
#######################################################
# Aggregated species results per cur scaler


### Corridors:

for cs in cscalers:
    cs = str(int(cs * 100))
    print(cs)

    df_all = pd.DataFrame()

    for i, species in species_dist.iterrows():
        sp = species.species
        sp = sp.replace(" ", "").lower()
        rds = os.path.join(swims, f'paths_{sp}_cs{cs}.rds')
        if not os.path.exists(rds):
            continue
        df = pyreadr.read_r(rds)
        df = df[None]

        # groupby patch for individual species
        df['prop_walks_success'] = df['count'] / df.reps
        df = df.groupby('patch').agg(
            X_m = ('X_m', 'first'),
            Y_m = ('Y_m', 'first'),
            prop_walks_success_avg = ('prop_walks_success', 'mean')
        ).reset_index()

        df_all = pd.concat([df_all, df], ignore_index=True)

    # groupby patch for all species
    def mean_cust_denom(x):
        s = x.sum()
        m = s/float(len(species_dist))
        return m
    df_all = df_all.groupby('patch').agg(
        X_m = ('X_m', 'first'),
        Y_m = ('Y_m', 'first'),
        prop_walks_success_avg = ('prop_walks_success_avg', mean_cust_denom)
    ).reset_index()

    gdf = gpd.GeoDataFrame(df_all, geometry=gpd.points_from_xy(df_all.X_m, df_all.Y_m), crs='EPSG:3005')

    geom_value = ((geom,value) for geom, value in zip(gdf.geometry, gdf.prop_walks_success_avg))
    rasterized = features.rasterize(geom_value,
                                    out_shape = template_rast.shape,
                                    fill = -9999,
                                    out = None,
                                    transform = template_rast.transform,
                                    all_touched = True,
                                    dtype = None)
    with rasterio.open(
            os.path.join(base, out, f'paths_ALL_cs{cs}.tif'), "w",
            driver = "GTiff",
            crs = template_rast.crs,
            transform = template_rast.transform,
            dtype = rasterio.float32,
            count = 1,
            width = template_rast.width,
            nodata = -9999,
            height = template_rast.height) as dst:
        dst.write(rasterized, indexes = 1)



### Pairwise connection strength
# get MPA centroids
mpas = gpd.read_file(gpkg_mpas, layer='mpas_05_dissolved')
mpas['cent_x'] = mpas.centroid.x
mpas['cent_y'] = mpas.centroid.y

for cs in cscalers:
    cs = str(int(cs * 100))
    print(cs)

    df_all = pd.DataFrame()

    for i, species in species_dist.iterrows():
        sp = species.species
        sp = sp.replace(" ", "").lower()
        rds = os.path.join(swims, f'paths_{sp}_cs{cs}.rds')
        if not os.path.exists(rds):
            continue
        df = pyreadr.read_r(rds)
        df = df[None]

        df['conn_strength'] = df.reps_success / df.reps
        df = df.groupby(['target', 'start']).agg(
            conn_strength = ('conn_strength', 'mean') # they should all be the same value per group
        ).reset_index()

        df_all = pd.concat([df_all, df], ignore_index=True)


    def mean_cust_denom(x):
        s = x.sum()
        m = s/float(len(species_dist))
        return m
    df_all = df_all.groupby(['target', 'start']).agg(
        conn_strength = ('conn_strength', mean_cust_denom) # they should all be the same value per group
    ).reset_index()


    # join centroid coordinates
    df_all = df_all.merge(mpas, left_on='start', right_on='gID_diss')
    df_all['fr_pt'] = [Point(xy) for xy in zip(df_all.cent_x, df_all.cent_y)]
    df_all = df_all.drop(columns=['geometry', 'gID_diss', 'cent_x', 'cent_y'])
    df_all = df_all.merge(mpas, left_on='target', right_on='gID_diss')
    df_all['to_pt'] = [Point(xy) for xy in zip(df_all.cent_x, df_all.cent_y)]
    df_all = df_all.drop(columns=['geometry', 'gID_diss', 'cent_x', 'cent_y'])

    # create lines between centroids
    df_all['line'] = df_all.apply(lambda row: LineString([row['fr_pt'], row['to_pt']]), axis=1)
    gdf = gpd.GeoDataFrame(df_all, geometry='line')
    gdf = gdf.drop(columns=['fr_pt', 'to_pt'])

    # out to geopackage
    gdf.to_file(gpkg_lines, layer=f'conns_ALL_cs{cs}', drive='GPKG')



#### Source and sink potential:
# Node in/out degree: count of connections and,
# Node in/out degree weighted: sum of connection strengths
        
# get MPA centroids
mpas = gpd.read_file(gpkg_mpas, layer='mpas_05_dissolved')
mpas['centroid'] = mpas.centroid
mpas = mpas.drop(columns=['geometry'])

for cs in cscalers:
    cs = str(int(cs * 100))
    print(cs)

    df_out_all = pd.DataFrame()
    df_in_all = pd.DataFrame()

    for i, species in species_dist.iterrows():
        sp = species.species
        sp = sp.replace(" ", "").lower()
        rds = os.path.join(swims, f'paths_{sp}_cs{cs}.rds')
        if not os.path.exists(rds):
            continue
        df = pyreadr.read_r(rds)
        df = df[None]

        df['conn_strength'] = df.reps_success / df.reps
        df = df.groupby(['target', 'start']).agg(
            conn_strength = ('conn_strength', 'mean')
        ).reset_index()
        df['i_count'] = 1

        df_out = df.groupby('start').agg(
            source_degree_weighted = ('conn_strength', 'sum'),
            source_degree =  ('i_count', 'sum')).reset_index()
        
        df_in = df.groupby('target').agg(
            sink_degree_weighted = ('conn_strength', 'sum'),
            sink_degree =  ('i_count', 'sum')).reset_index()
        
        df_out_all = pd.concat([df_out_all, df_out], ignore_index=True)
        df_in_all = pd.concat([df_in_all, df_in], ignore_index=True)


    df_out_all = df_out_all.groupby('start').agg(
        source_degree_weighted = ('source_degree_weighted', 'sum'),
        source_degree =  ('source_degree', 'sum')).reset_index()
    df_in_all = df_in_all.groupby('target').agg(
        sink_degree_weighted = ('sink_degree_weighted', 'sum'),
        sink_degree =  ('sink_degree', 'sum')).reset_index()

    mpas_ss = mpas.merge(df_out_all, left_on='gID_diss', right_on='start', how='left')
    mpas_ss = mpas_ss.merge(df_in_all, left_on='gID_diss', right_on='target', how='left')

    gdf = gpd.GeoDataFrame(mpas_ss, geometry='centroid')
    gdf = gdf.drop(columns=['start', 'target'])
    gdf.to_file(gpkg_pts, layer=f's_ALL_cs{cs}', drive='GPKG')





#######################################################
#######################################################
# Aggregated species results by threshold and cur scaler


### Corridors:

for cs in cscalers:
    cs = str(int(cs * 100))
    print(cs)

    for th in thresholds:
        sd = species_dist[species_dist.movement_bin == th]

        df_all = pd.DataFrame()

        for i, species in sd.iterrows():
            sp = species.species
            sp = sp.replace(" ", "").lower()
            rds = os.path.join(swims, f'paths_{sp}_cs{cs}.rds')
            if not os.path.exists(rds):
                continue
            df = pyreadr.read_r(rds)
            df = df[None]

            # groupby patch for individual species
            df['prop_walks_success'] = df['count'] / df.reps
            df = df.groupby('patch').agg(
                X_m = ('X_m', 'first'),
                Y_m = ('Y_m', 'first'),
                prop_walks_success_avg = ('prop_walks_success', 'mean')
            ).reset_index()

            df_all = pd.concat([df_all, df], ignore_index=True)

        # groupby patch for all species
        def mean_cust_denom(x):
            s = x.sum()
            m = s/float(len(sd))
            return m
        df_all = df_all.groupby('patch').agg(
            X_m = ('X_m', 'first'),
            Y_m = ('Y_m', 'first'),
            prop_walks_success_avg = ('prop_walks_success_avg', mean_cust_denom)
        ).reset_index()

        gdf = gpd.GeoDataFrame(df_all, geometry=gpd.points_from_xy(df_all.X_m, df_all.Y_m), crs='EPSG:3005')

        geom_value = ((geom,value) for geom, value in zip(gdf.geometry, gdf.prop_walks_success_avg))
        rasterized = features.rasterize(geom_value,
                                        out_shape = template_rast.shape,
                                        fill = -9999,
                                        out = None,
                                        transform = template_rast.transform,
                                        all_touched = True,
                                        dtype = None)
        with rasterio.open(
                os.path.join(base, out, f'paths_ALL_th{th}_cs{cs}.tif'), "w",
                driver = "GTiff",
                crs = template_rast.crs,
                transform = template_rast.transform,
                dtype = rasterio.float32,
                count = 1,
                width = template_rast.width,
                nodata = -9999,
                height = template_rast.height) as dst:
            dst.write(rasterized, indexes = 1)



### Pairwise connection strength
# get MPA centroids
mpas = gpd.read_file(gpkg_mpas, layer='mpas_05_dissolved')
mpas['cent_x'] = mpas.centroid.x
mpas['cent_y'] = mpas.centroid.y

for cs in cscalers:
    cs = str(int(cs * 100))
    print(cs)

    for th in thresholds:
        sd = species_dist[species_dist.movement_bin == th]

        df_all = pd.DataFrame()

        for i, species in sd.iterrows():
            sp = species.species
            sp = sp.replace(" ", "").lower()
            rds = os.path.join(swims, f'paths_{sp}_cs{cs}.rds')
            if not os.path.exists(rds):
                continue
            df = pyreadr.read_r(rds)
            df = df[None]

            df['conn_strength'] = df.reps_success / df.reps
            df = df.groupby(['target', 'start']).agg(
                conn_strength = ('conn_strength', 'mean') # they should all be the same value per group
            ).reset_index()

            df_all = pd.concat([df_all, df], ignore_index=True)


        def mean_cust_denom(x):
            s = x.sum()
            m = s/float(len(sd))
            return m
        df_all = df_all.groupby(['target', 'start']).agg(
            conn_strength = ('conn_strength', mean_cust_denom) # they should all be the same value per group
        ).reset_index()


        # join centroid coordinates
        df_all = df_all.merge(mpas, left_on='start', right_on='gID_diss')
        df_all['fr_pt'] = [Point(xy) for xy in zip(df_all.cent_x, df_all.cent_y)]
        df_all = df_all.drop(columns=['geometry', 'gID_diss', 'cent_x', 'cent_y'])
        df_all = df_all.merge(mpas, left_on='target', right_on='gID_diss')
        df_all['to_pt'] = [Point(xy) for xy in zip(df_all.cent_x, df_all.cent_y)]
        df_all = df_all.drop(columns=['geometry', 'gID_diss', 'cent_x', 'cent_y'])

        # create lines between centroids
        df_all['line'] = df_all.apply(lambda row: LineString([row['fr_pt'], row['to_pt']]), axis=1)
        gdf = gpd.GeoDataFrame(df_all, geometry='line')
        gdf = gdf.drop(columns=['fr_pt', 'to_pt'])

        # out to geopackage
        gdf.to_file(gpkg_lines, layer=f'conns_ALL_th{th}_cs{cs}', drive='GPKG')



#### Source and sink potential:
# Node in/out degree: count of connections and,
# Node in/out degree weighted: sum of connection strengths
        
# get MPA centroids
mpas = gpd.read_file(gpkg_mpas, layer='mpas_05_dissolved')
mpas['centroid'] = mpas.centroid
mpas = mpas.drop(columns=['geometry'])

for cs in cscalers:
    cs = str(int(cs * 100))
    print(cs)

    for th in thresholds:
        sd = species_dist[species_dist.movement_bin == th]

        df_out_all = pd.DataFrame()
        df_in_all = pd.DataFrame()

        for i, species in sd.iterrows():
            sp = species.species
            sp = sp.replace(" ", "").lower()
            rds = os.path.join(swims, f'paths_{sp}_cs{cs}.rds')
            if not os.path.exists(rds):
                continue
            df = pyreadr.read_r(rds)
            df = df[None]

            df['conn_strength'] = df.reps_success / df.reps
            df = df.groupby(['target', 'start']).agg(
                conn_strength = ('conn_strength', 'mean')
            ).reset_index()
            df['i_count'] = 1

            df_out = df.groupby('start').agg(
                source_degree_weighted = ('conn_strength', 'sum'),
                source_degree =  ('i_count', 'sum')).reset_index()
            
            df_in = df.groupby('target').agg(
                sink_degree_weighted = ('conn_strength', 'sum'),
                sink_degree =  ('i_count', 'sum')).reset_index()
            
            df_out_all = pd.concat([df_out_all, df_out], ignore_index=True)
            df_in_all = pd.concat([df_in_all, df_in], ignore_index=True)


        df_out_all = df_out_all.groupby('start').agg(
            source_degree_weighted = ('source_degree_weighted', 'sum'),
            source_degree =  ('source_degree', 'sum')).reset_index()
        df_in_all = df_in_all.groupby('target').agg(
            sink_degree_weighted = ('sink_degree_weighted', 'sum'),
            sink_degree =  ('sink_degree', 'sum')).reset_index()

        mpas_ss = mpas.merge(df_out_all, left_on='gID_diss', right_on='start', how='left')
        mpas_ss = mpas_ss.merge(df_in_all, left_on='gID_diss', right_on='target', how='left')

        gdf = gpd.GeoDataFrame(mpas_ss, geometry='centroid')
        gdf = gdf.drop(columns=['start', 'target'])
        gdf.to_file(gpkg_pts, layer=f's_ALL_th{th}_cs{cs}', drive='GPKG')