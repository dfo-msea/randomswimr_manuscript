

# Test sensitivity of number of reps.

# Two species, one broad, one restricted, all thresholds for each species
# arrowtooth flounder, 75
# butter sole, 75



import pandas as pd
import geopandas as gpd
import pyreadr
import rasterio
from rasterio import features
import os
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

base = r'C:\Users\cristianij\Documents\Projects\randomswimr\randomswimr_manuscript'
swims = os.path.join(base, 'analysis/spatial/outputs_20240111_sens_reps/outputs_swims')
template_rast = rasterio.open(os.path.join(base, 'analysis/spatial/original_inputs/current_EW.tif'))
out = os.path.join(base, 'analysis/spatial/outputs_20240111_sens_reps/outputs_processed')
species_dist = pd.read_csv(os.path.join(base, 'analysis/spatial/species_movement.csv'),  encoding = "ISO-8859-1")
mpas_distance = pd.read_csv(os.path.join(base, 'analysis/spatial/mpas_distance.csv'))


reps_list = [10, 50, 75, 100, 150, 200, 250, 300]
thresholds = [10,100,500,1000]
species_mpas = {'Arrowtooth Flounder': [75],
                'Butter Sole': [75]}



###################################
# Get list of connections and count
# Calculate similarity of lists between steps

conns = pd.DataFrame()

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()

    for th in thresholds:
        th = str(th)

        for ms in reps_list:
            ms = str(ms)

            rds = os.path.join(swims, f'paths_{sp}_thresh{th}_reps{ms}.rds')
            if not os.path.exists(rds):
                continue
            df = pyreadr.read_r(rds)
            df = df[None]
            
            df['conn_strength'] = df.reps_success / df.reps

            df = df.groupby(['start', 'target']).agg(
                conn_strength = ('conn_strength', 'mean')
            ).reset_index()

            df['reps'] = int(ms)
            df['species'] = sp
            df['threshold'] = int(th)

            conns = pd.concat([conns, df], ignore_index=True)



# Calculate similarity to a list of all MPAs within range
similarity = pd.DataFrame()

for spe in species_mpas:

    sp = spe.replace(" ", "").lower()

    for th in thresholds:

        # get list of all possible destination MPAs
        dmpas = mpas_distance[(mpas_distance.gID_diss_ORIG.isin(species_mpas[spe])) & (mpas_distance.PathCost <= th*1000)]
        dmpas = dmpas.gID_diss_DEST.tolist()

        for ms in reps_list:
            targs = conns[(conns.reps == ms) & (conns.species == sp) & (conns.threshold == th)].target.unique().tolist()

            if len(targs)>0:
                # common elements
                com = set(dmpas).intersection(set(targs))
                num_com = len(com)
                # Total unique elements in both lists
                tot = set(dmpas).union(set(targs))
                num_tot = len(tot)

                # % similarity
                perc_sim = (num_com / num_tot) * 100

                df = pd.DataFrame({'Repetitions':[ms],
                                'Percent similar to all possible connections':[perc_sim], 
                                'species':[sp],
                                'threshold':[th]})
                similarity = pd.concat([similarity, df], ignore_index=True)
            else:
                df = pd.DataFrame({'Repetitions':[ms],
                                'Percent similar to all possible connections':0, 
                                'species':[sp],
                                'threshold':[th]})
                similarity = pd.concat([similarity, df], ignore_index=True)                

similarity['species_threshold'] = similarity.species + '_' + (similarity.threshold).astype(str)


palette = sns.color_palette("hls", 4)
t = sns.lineplot(data=similarity, 
                 x='Repetitions', y='Percent similar to all possible connections', 
                 hue='threshold', units='species_threshold', style='species', estimator=None, marker='o',palette=palette)
t.figure.savefig(os.path.join(out, f'conns_similarityAll_reps_h.png'))



###################################


# Rasters of cells passed through

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()

    for th in thresholds:
        th = str(th)

        for ms in reps_list:
            ms = str(ms)

            rds = os.path.join(swims, f'paths_{sp}_thresh{th}_reps{ms}.rds')
            if not os.path.exists(rds):
                continue
            df = pyreadr.read_r(rds)
            df = df[None]

            df['prop_walks_success'] = df['count'] / df.reps
            df = df.groupby('patch').agg(
                X_m = ('X_m', 'first'),
                Y_m = ('Y_m', 'first'),
                prop_walks_success_avg = ('prop_walks_success', 'mean')
            ).reset_index()
            gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.X_m, df.Y_m), crs='EPSG:3005')

            geom_value = ((geom,value) for geom, value in zip(gdf.geometry, gdf.prop_walks_success_avg))
            rasterized = features.rasterize(geom_value,
                                            out_shape = template_rast.shape,
                                            fill = -9999,
                                            out = None,
                                            transform = template_rast.transform,
                                            all_touched = True,
                                            dtype = None)
            with rasterio.open(
                    os.path.join(base, out, f'paths_{sp}_thresh{th}_reps{ms}.tif'), "w",
                    driver = "GTiff",
                    crs = template_rast.crs,
                    transform = template_rast.transform,
                    dtype = rasterio.float32,
                    count = 1,
                    width = template_rast.width,
                    nodata = -9999,
                    height = template_rast.height) as dst:
                dst.write(rasterized, indexes = 1)


### Raster comparison

df_cc = pd.DataFrame()

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()

    for th in thresholds:
        th = str(th)

        # first read in 300 reps rasters to use as comparison
        tif = os.path.join(out, f'paths_{sp}_thresh{th}_reps{300}.tif')
        with rasterio.open(tif, 'r') as ds:
            arr300 = ds.read()#[0]
            arr300 = arr300.flatten()

            # Need to flatten or else it is treating rows as variables and the returned raster isn't
            # relevant to comparing the overall raster.

        for ms in reps_list:
            
            if ms==300:
                continue
            ms = str(ms)

            tif = os.path.join(out, f'paths_{sp}_thresh{th}_reps{ms}.tif')
            if not os.path.exists(tif):
                continue
            with rasterio.open(tif, 'r') as ds:
                arr = ds.read()#[0]
                arr = arr.flatten()

            cc_m = np.corrcoef(arr300, arr)
            cc = cc_m[0][1]
            # From arcgis: "The correlation matrix shows the values of the correlation coefficients that depict the relationship between two datasets. In the case of a set of raster layers, the correlation matrix presents the cell values from one raster layer as they relate to the cell values of another layer."
            if np.isnan(cc):
                cc = 1 # if there is no variance (e.g. all values are 1), then numpy outputs nan
            
            df = pd.DataFrame({'reps':[ms],
                'cc':[cc], 
                'species':[sp],
                'threshold':[th],
                'fuv':[1 - cc**2]})

            df_cc = pd.concat([df_cc, df], ignore_index=True)

df_cc['species_threshold'] = df_cc.species + '_' + (df_cc.threshold).astype(str)
# palette = sns.color_palette("hls", 4)
# h = sns.lineplot(data=df_cc, 
#                  x='reps', y='cc', 
#                  hue='threshold', units='species_threshold', style='species', estimator=None, marker='o',palette=palette)
# h.figure.savefig(os.path.join(out, f'reps_rast_similarity.png'))

# so what's tricky about this is that I think numpy is only comparing where values area not None.
# Therefore for the 10km threshold with fewer steps, there are fewer possible cells that can be
# covered, so a few cell difference here is a higher proportion of distance than for a larger raster.
# The important thing is that we see a plateau, which we mostly do.
# However...

# Now try a version that first turns the nodata values to 0.

df_cc = pd.DataFrame()

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()

    for th in thresholds:
        th = str(th)

        # first read in 300 reps rasters to use as comparison
        tif = os.path.join(out, f'paths_{sp}_thresh{th}_reps{300}.tif')
        with rasterio.open(tif, 'r') as ds:
            arr300 = ds.read()#[0]
            arr300 = arr300.flatten()
            arr300 = np.where(arr300==-9999, 0, arr300)

            # Need to flatten or else it is treating rows as variables and the returned raster isn't
            # relevant to comparing the overall raster.

        for ms in reps_list:
            
            if ms==300:
                continue
            ms = str(ms)

            tif = os.path.join(out, f'paths_{sp}_thresh{th}_reps{ms}.tif')
            if not os.path.exists(tif):
                continue
            with rasterio.open(tif, 'r') as ds:
                arr = ds.read()#[0]
                arr = arr.flatten()
                arr = np.where(arr==-9999, 0, arr)

            cc_m = np.corrcoef(arr300, arr)
            cc = cc_m[0][1]
            # From arcgis: "The correlation matrix shows the values of the correlation coefficients that depict the relationship between two datasets. In the case of a set of raster layers, the correlation matrix presents the cell values from one raster layer as they relate to the cell values of another layer."
            if np.isnan(cc):
                cc = 1 # if there is no variance (e.g. all values are 1), then numpy outputs nan
            
            df = pd.DataFrame({'Repetitions':[ms],
                'Correlation coefficient':[cc], 
                'species':[sp],
                'threshold':[th],
                'fuv':[1 - cc**2]})

            df_cc = pd.concat([df_cc, df], ignore_index=True)

df_cc['species_threshold'] = df_cc.species + '_' + (df_cc.threshold).astype(str)

# palette = sns.color_palette("hls", 4)
# r = sns.lineplot(data=df_cc, 
#                  x='reps', y='fuv', 
#                  hue='threshold', units='species_threshold', style='species', estimator=None, marker='o',palette=palette)
# r.set(ylabel='Fraction of unexplained variance (with 300 reps)')
# r.figure.savefig(os.path.join(out, f'reps_rast_similarity_0.png'))

palette = sns.color_palette("hls", 4)
n = sns.lineplot(data=df_cc, 
                 x='reps', y='cc', 
                 hue='threshold', units='species_threshold', style='species', estimator=None, marker='o',palette=palette)
n.set(ylabel='Correlation coefficiente (with 300 reps)')
n.figure.savefig(os.path.join(out, f'reps_rast_similarity_0cc.png'))


# plot together
fig, (ax1, ax2) = plt.subplots(figsize=(7, 3.5), ncols=2, sharex=False, sharey=False)
sns.set_context("paper")
palette = sns.color_palette("hls", 4)
t = sns.lineplot(data=similarity, 
                 x='Repetitions', y='Percent similar to all possible connections', 
                 hue='threshold', units='species_threshold', style='species', estimator=None, 
                 marker='o',palette=palette, ax=ax1)
t.get_legend().remove()
n = sns.lineplot(data=df_cc, 
                 x='Repetitions', y='Correlation coefficient', 
                 hue='threshold', units='species_threshold', style='species', estimator=None, 
                 marker='o',palette=palette, ax=ax2)
n.set(ylabel='Correlation coefficient (with 300 reps)')
fig.tight_layout()
fig.figure.savefig(os.path.join(out, f'reps_combine.png'))


# Interpretation:
# We will go with 150 reps.
# We viewed the rasters for buttersole, 100 threshold, 100 vs 150 vs 300 reps.
# I thought 100 looked fine. The added swims on 300 reps don't seem that significant and frequent,
# but Patrick thought they were. However, he said 150 looked a lot better (although it didn't seem 
# like that much of an improvement from 100). I'm happy to go with 150 though.
