
# Compare rasters
# Compare number of MPAs connected



import pandas as pd
import geopandas as gpd
import pyreadr
import rasterio
from rasterio import features
import os
import seaborn as sns
import matplotlib.pyplot as plt

base = r'C:\Users\cristianij\Documents\Projects\randomswimr\randomswimr_manuscript'
swims = os.path.join(base, 'analysis/spatial/outputs_20240109_sens_steps/outputs_swims')
template_rast = rasterio.open(os.path.join(base, 'analysis/spatial/original_inputs/current_EW.tif'))
out = os.path.join(base, 'analysis/spatial/outputs_20240109_sens_steps/outputs_processed')
species_dist = pd.read_csv(os.path.join(base, 'analysis/spatial/species_movement.csv'),  encoding = "ISO-8859-1")
mpas_distance = pd.read_csv(os.path.join(base, 'analysis/spatial/mpas_distance.csv'))


maxsteps = [50,100,500,1000,2500,5000,10000]
species_mpas = {'Longnose Skate': [54],
                'Flathead Sole': [54],
                'Arrowtooth Flounder': [54],
                'Yelloweye Rockfish': [51, 75]}


###################################
# Rasters of cells passed through

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()
    print(sp)

    for ms in maxsteps:
        ms = str(ms)

        rds = os.path.join(swims, f'paths_{sp}_cs25_steps{ms}.rds')
        if not os.path.exists(rds):
            continue
        df = pyreadr.read_r(rds)
        df = df[None]

        df['prop_walks_success'] = df['count'] / df.reps
        df = df.groupby('patch').agg(
            X_m = ('X_m', 'first'),
            Y_m = ('Y_m', 'first'),
            prop_walks_success_avg = ('prop_walks_success', 'mean')
        ).reset_index()
        gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.X_m, df.Y_m), crs='EPSG:3005')

        geom_value = ((geom,value) for geom, value in zip(gdf.geometry, gdf.prop_walks_success_avg))
        rasterized = features.rasterize(geom_value,
                                        out_shape = template_rast.shape,
                                        fill = -9999,
                                        out = None,
                                        transform = template_rast.transform,
                                        all_touched = True,
                                        dtype = None)
        with rasterio.open(
                os.path.join(base, out, f'paths_{sp}_steps{ms}.tif'), "w",
                driver = "GTiff",
                crs = template_rast.crs,
                transform = template_rast.transform,
                dtype = rasterio.float32,
                count = 1,
                width = template_rast.width,
                nodata = -9999,
                height = template_rast.height) as dst:
            dst.write(rasterized, indexes = 1)

# It doesn't make sense to do a raster comparison between steps here. We expect them to be different
# with more steps. Won't do a pearson correlation and FUV analysis here. However, it will make
# sense to do when testing sensitivity of reps.


###################################
# Get list of connections and count (can just be done from dataframe, don't need to create lines)
# Calculate similarity of lists between steps

conns = pd.DataFrame()

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()
    print(sp)

    for ms in maxsteps:
        ms = str(ms)

        rds = os.path.join(swims, f'paths_{sp}_cs25_steps{ms}.rds')
        if not os.path.exists(rds):
            continue
        df = pyreadr.read_r(rds)
        df = df[None]
        
        df['conn_strength'] = df.reps_success / df.reps
        df = df.groupby(['start', 'target']).agg(
            conn_strength = ('conn_strength', 'mean')
        ).reset_index()

        df['steps'] = int(ms)
        df['species'] = sp

        conns = pd.concat([conns, df], ignore_index=True)

# Calculate similarity to 10,000 steps
# https://www.geeksforgeeks.org/python-percentage-similarity-of-lists/

similarity = pd.DataFrame()

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()
    print(sp)

    # get lists of targets for 10,000 steps, assuming this achieves the most connections
    targets_10k = conns[(conns.steps == 10000) & (conns.species == sp)].target.unique().tolist()

    for ms in maxsteps:
        targs = conns[(conns.steps == ms) & (conns.species == sp)].target.unique().tolist()

        # common elements
        com = set(targets_10k).intersection(set(targs))
        num_com = len(com)
        # Total unique elements in both lists
        tot = set(targets_10k).union(set(targs))
        num_tot = len(tot)

        # % similarity
        perc_sim = (num_com / num_tot) * 100

        df = pd.DataFrame({'steps':[ms], 'percent_sim_to_10k':[perc_sim], 'species':[sp]})
        similarity = pd.concat([similarity, df], ignore_index=True)

# Plot
g = sns.lineplot(data=similarity, x='steps', y='percent_sim_to_10k', hue='species', marker='o')
g.figure.savefig(os.path.join(out, f'conns_similarity10k.png'))


# Calculate similarity to a list of all MPAs within range
similarity = pd.DataFrame()

for sp in species_mpas:

    # get list of all possible destination MPAs
    dist = species_dist[species_dist.species==sp].movement_bin.values[0]
    dmpas = mpas_distance[(mpas_distance.gID_diss_ORIG.isin(species_mpas[sp])) & (mpas_distance.PathCost <= dist*1000)]
    dmpas = dmpas.gID_diss_DEST.tolist()

    sp = sp.replace(" ", "").lower()

    for ms in maxsteps:
        targs = conns[(conns.steps == ms) & (conns.species == sp)].target.unique().tolist()

        # common elements
        com = set(dmpas).intersection(set(targs))
        num_com = len(com)
        # Total unique elements in both lists
        tot = set(dmpas).union(set(targs))
        num_tot = len(tot)

        # % similarity
        perc_sim = (num_com / num_tot) * 100

        # staright line steps of threshold (assuming a 3km step and no diagonal movement)
        sl_steps_perc = ms / (dist / 3) * 100

        df = pd.DataFrame({'steps':[ms],
                           'percent_sim_to_all':[perc_sim], 
                           'species':[sp],
                           'percent_of_straightline_steps': sl_steps_perc})
        similarity = pd.concat([similarity, df], ignore_index=True)

g = sns.lineplot(data=similarity, x='steps', y='percent_sim_to_all', hue='species', marker='o')
g.figure.savefig(os.path.join(out, f'conns_similarityAll.png'))


# x axis of max steps divided by straighline steps of threshold
p = sns.relplot(data=similarity, 
                x='percent_of_straightline_steps', 
                y='percent_sim_to_all', 
                col='species', col_wrap=2, 
                kind='line', marker='o',
                facet_kws=dict(sharex=False))
p.axes[0,0].set_xlim(0,3000)
p.axes[0,1].set_xlim(0,6000)
p.axes[1,0].set_xlim(0,30000)
p.axes[1,1].set_xlim(0,300000)
p.figure.savefig(os.path.join(out, f'conns_similarityAll_slsteps.png'))

# put on one plot and limit x axis
p = sns.lineplot(data=similarity, 
                x='percent_of_straightline_steps', 
                y='percent_sim_to_all', 
                hue='species', marker='o')
p.set_xlim(0,5000)
p.figure.savefig(os.path.join(out, f'conns_similarityAll_slsteps_xlim.png'))

# Interpretation:
# We may want to set maxsteps to vary by species. We can see that the y axis saturates at about
# 3000%. This gives us ONE number to refer to.
# This would be:
# 10,000 steps for 1000km threshold
# 5000 steps for 500km threshold
# 1000 steps for 100km threshold
# 100 steps for 10km threshold

# However, now Patrick is saying that we could just do 1000% the threshold distance. We don't
# necessarily need to be looking for saturation, which may not even make sense. We could instead
# reason from the threshold distance and limit too much swimming around.

