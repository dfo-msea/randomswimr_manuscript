

# Test sensitivity of current scalers.

# Two species, one broad, one restricted, all thresholds for each species
# arrowtooth flounder, 75
# butter sole, 75



import pandas as pd
import geopandas as gpd
import pyreadr
import rasterio
from rasterio import features
import os
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

base = r'C:\Users\cristianij\Documents\Projects\randomswimr\randomswimr_manuscript'
swims = os.path.join(base, 'analysis/spatial/outputs_20240112_sens_cs/outputs_swims')
template_rast = rasterio.open(os.path.join(base, 'analysis/spatial/original_inputs/current_EW.tif'))
out = os.path.join(base, 'analysis/spatial/outputs_20240112_sens_cs/outputs_processed')
species_dist = pd.read_csv(os.path.join(base, 'analysis/spatial/species_movement.csv'),  encoding = "ISO-8859-1")
mpas_distance = pd.read_csv(os.path.join(base, 'analysis/spatial/mpas_distance.csv'))


cs_list = [0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6]
thresholds = [10,100,500,1000]
species_mpas = {'Arrowtooth Flounder': [75],
                'Butter Sole': [75]}



###################################
# Get list of connections and count
# Calculate similarity of lists between steps

conns = pd.DataFrame()

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()

    for th in thresholds:
        th = str(th)

        for ms in cs_list:
            ms = str(int(ms*100))

            rds = os.path.join(swims, f'paths_{sp}_cs{ms}_thresh{th}.rds')
            if not os.path.exists(rds):
                continue
            df = pyreadr.read_r(rds)
            df = df[None]
            
            df['conn_strength'] = df.reps_success / df.reps

            df = df.groupby(['start', 'target']).agg(
                conn_strength = ('conn_strength', 'mean')
            ).reset_index()

            df['cs'] = int(ms)/100
            df['species'] = sp
            df['threshold'] = int(th)

            conns = pd.concat([conns, df], ignore_index=True)



# Calculate similarity to a list of all MPAs within range
similarity = pd.DataFrame()

for spe in species_mpas:

    sp = spe.replace(" ", "").lower()

    for th in thresholds:

        # get list of all possible destination MPAs
        dmpas = mpas_distance[(mpas_distance.gID_diss_ORIG.isin(species_mpas[spe])) & (mpas_distance.PathCost <= th*1000)]
        dmpas = dmpas.gID_diss_DEST.tolist()

        for ms in cs_list:
            targs = conns[(conns.cs == ms) & (conns.species == sp) & (conns.threshold == th)].target.unique().tolist()

            if len(targs)>0:
                # common elements
                com = set(dmpas).intersection(set(targs))
                num_com = len(com)
                # Total unique elements in both lists
                tot = set(dmpas).union(set(targs))
                num_tot = len(tot)

                # % similarity
                perc_sim = (num_com / num_tot) * 100

                df = pd.DataFrame({'Current scaler':[ms],
                                'Percent similar to all possible connections':[perc_sim], 
                                'species':[sp],
                                'threshold':[th]})
                similarity = pd.concat([similarity, df], ignore_index=True)
            else:
                df = pd.DataFrame({'Current scaler':[ms],
                                'Percent similar to all possible connections':0, 
                                'species':[sp],
                                'threshold':[th]})
                similarity = pd.concat([similarity, df], ignore_index=True)                

similarity['species_threshold'] = similarity.species + '_' + (similarity.threshold).astype(str)


# p = sns.lineplot(data=similarity, 
#                  x='cs', y='percent_sim_to_all', 
#                  hue='species', units='species_threshold', estimator=None, marker='o')
# p.figure.savefig(os.path.join(out, f'conns_similarityAll_cs.png'))

palette = sns.color_palette("hls", 4)
t = sns.lineplot(data=similarity, 
                 x='Current scaler', y='Percent similar to all possible connections', 
                 hue='threshold', units='species_threshold', style='species', estimator=None, marker='o',palette=palette)
sns.move_legend(t, "upper left", bbox_to_anchor=(1, 1))
t.figure.savefig(os.path.join(out, f'conns_similarityAll_cs_h.png'))



###################################


# Rasters of cells passed through

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()

    for th in thresholds:
        th = str(th)

        for ms in cs_list:
            ms = str(int(ms*100))

            rds = os.path.join(swims, f'paths_{sp}_cs{ms}_thresh{th}.rds')
            if not os.path.exists(rds):
                continue
            df = pyreadr.read_r(rds)
            df = df[None]

            df['prop_walks_success'] = df['count'] / df.reps
            df = df.groupby('patch').agg(
                X_m = ('X_m', 'first'),
                Y_m = ('Y_m', 'first'),
                prop_walks_success_avg = ('prop_walks_success', 'mean')
            ).reset_index()
            gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.X_m, df.Y_m), crs='EPSG:3005')

            geom_value = ((geom,value) for geom, value in zip(gdf.geometry, gdf.prop_walks_success_avg))
            rasterized = features.rasterize(geom_value,
                                            out_shape = template_rast.shape,
                                            fill = -9999,
                                            out = None,
                                            transform = template_rast.transform,
                                            all_touched = True,
                                            dtype = None)
            with rasterio.open(
                    os.path.join(base, out, f'paths_{sp}_cs{ms}_thresh{th}.tif'), "w",
                    driver = "GTiff",
                    crs = template_rast.crs,
                    transform = template_rast.transform,
                    dtype = rasterio.float32,
                    count = 1,
                    width = template_rast.width,
                    nodata = -9999,
                    height = template_rast.height) as dst:
                dst.write(rasterized, indexes = 1)


### Raster comparison

# df_cc = pd.DataFrame()

# for sp in species_mpas:
#     sp = sp.replace(" ", "").lower()

#     for th in thresholds:
#         th = str(th)

#         # first read in 0.6 cs raster to use as comparison
#         tif = os.path.join(out, f'paths_{sp}_cs{60}_thresh{th}.tif')
#         with rasterio.open(tif, 'r') as ds:
#             arr60 = ds.read()#[0]
#             arr60 = arr60.flatten()

#             # Need to flatten or else it is treating rows as variables and the returned raster isn't
#             # relevant to comparing the overall raster.

#         for ms in cs_list:
            
#             if ms==0.6:
#                 continue
#             ms = str(int(ms*100)) 

#             tif = os.path.join(out, f'paths_{sp}_cs{ms}_thresh{th}.tif')
#             if not os.path.exists(tif):
#                 continue
#             with rasterio.open(tif, 'r') as ds:
#                 arr = ds.read()#[0]
#                 arr = arr.flatten()

#             cc_m = np.corrcoef(arr60, arr)
#             cc = cc_m[0][1]
#             # From arcgis: "The correlation matrix shows the values of the correlation coefficients that depict the relationship between two datasets. In the case of a set of raster layers, the correlation matrix presents the cell values from one raster layer as they relate to the cell values of another layer."
#             if np.isnan(cc):
#                 cc = 1 # if there is no variance (e.g. all values are 1), then numpy outputs nan
            
#             df = pd.DataFrame({'cs':[int(ms)/100],
#                 'cc':[cc], 
#                 'species':[sp],
#                 'threshold':[th],
#                 'fuv':[1 - cc**2]})

#             df_cc = pd.concat([df_cc, df], ignore_index=True)

# df_cc['species_threshold'] = df_cc.species + '_' + (df_cc.threshold).astype(str)
# palette = sns.color_palette("hls", 4)
# h = sns.lineplot(data=df_cc, 
#                  x='cs', y='cc', 
#                  hue='threshold', units='species_threshold', style='species', estimator=None, marker='o',palette=palette)
# sns.move_legend(h, "upper left", bbox_to_anchor=(1, 1))
# h.figure.savefig(os.path.join(out, f'cs_rast_similarity.png'))

# so what's tricky about this is that I think numpy is only comparing where values area not None.
# Therefore for the 10km threshold with fewer steps, there are fewer possible cells that can be
# covered, so a few cell difference here is a higher proportion of distance than for a larger raster.
# The important thing is that we see a plateau, which we mostly do.
# However...

# Now try a version that first turns the nodata values to 0.

df_cc = pd.DataFrame()

for sp in species_mpas:
    sp = sp.replace(" ", "").lower()

    for th in thresholds:
        th = str(th)

        # first read in 300 reps rasters to use as comparison
        tif = os.path.join(out, f'paths_{sp}_cs{60}_thresh{th}.tif')
        with rasterio.open(tif, 'r') as ds:
            arr60 = ds.read()#[0]
            arr60 = arr60.flatten()
            arr60 = np.where(arr60==-9999, 0, arr60)

            # Need to flatten or else it is treating rows as variables and the returned raster isn't
            # relevant to comparing the overall raster.

        for ms in cs_list:
            
            if ms==0.6:
                continue
            ms = str(int(ms*100))

            tif = os.path.join(out, f'paths_{sp}_cs{ms}_thresh{th}.tif')
            if not os.path.exists(tif):
                continue
            with rasterio.open(tif, 'r') as ds:
                arr = ds.read()#[0]
                arr = arr.flatten()
                arr = np.where(arr==-9999, 0, arr)

            cc_m = np.corrcoef(arr60, arr)
            cc = cc_m[0][1]
            # From arcgis: "The correlation matrix shows the values of the correlation coefficients that depict the relationship between two datasets. In the case of a set of raster layers, the correlation matrix presents the cell values from one raster layer as they relate to the cell values of another layer."
            if np.isnan(cc):
                cc = 1 # if there is no variance (e.g. all values are 1), then numpy outputs nan
            
            df = pd.DataFrame({'Current scaler':[int(ms)/100],
                'cc':[cc], 
                'species':[sp],
                'threshold':[th],
                'fuv':[1 - cc**2]})

            df_cc = pd.concat([df_cc, df], ignore_index=True)

df_cc['species_threshold'] = df_cc.species + '_' + (df_cc.threshold).astype(str)


palette = sns.color_palette("hls", 4)
r = sns.lineplot(data=df_cc, 
                 x='Current scaler', y='fuv', 
                 hue='threshold', units='species_threshold', style='species', estimator=None, marker='o',palette=palette)
sns.move_legend(r, "upper left", bbox_to_anchor=(1, 1))
r.set(ylabel='Fraction of unexplained variance (with 0.6 cs)')
r.figure.savefig(os.path.join(out, f'cs_rast_similarity_0.png'))

palette = sns.color_palette("hls", 4)
n = sns.lineplot(data=df_cc, 
                 x='Current scaler', y='cc', 
                 hue='threshold', units='species_threshold', style='species', estimator=None, marker='o',palette=palette)
n.set(ylabel='Correlation coefficiente (with 0.6 cs)')
sns.move_legend(n, "upper left", bbox_to_anchor=(1, 1))
n.figure.savefig(os.path.join(out, f'cs_rast_similarity_0cc.png'))


# plot together
fig, (ax1, ax2) = plt.subplots(figsize=(7, 3.5), ncols=2, sharex=False, sharey=False)
sns.set_context("paper")
palette = sns.color_palette("hls", 4)
t = sns.lineplot(data=similarity, 
                 x='Current scaler', y='Percent similar to all possible connections', 
                 hue='threshold', units='species_threshold', style='species', estimator=None, 
                 marker='o',palette=palette, ax=ax1)
t.get_legend().remove()
n = sns.lineplot(data=df_cc, 
                 x='Current scaler', y='cc', 
                 hue='threshold', units='species_threshold', style='species', estimator=None, 
                 marker='o',palette=palette, ax=ax2)
n.set(ylabel='Correlation coefficient (with 0.6 scaler)')
fig.tight_layout()
fig.figure.savefig(os.path.join(out, f'cs_combine.png'))




# Interpretation:
# We will go with 0, 0.1, and 0.3
# Things are mostly saturated at 0.3. It's unlikley that currents would dominate so much over
# habitat preference, therefore, if things are mostly saturated by 0.3, then we should just go with
# that instead of stretching it to 0.6.
# We will do 0.1 because that is somewhat at 50% saturation (at least for purple and blue dashed
# lines). We'd rather do a bit below 50% than over 50.
# We want to use 0 so that we can compare to just the influence of habitat preference.
