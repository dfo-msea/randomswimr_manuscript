
# Species trait/threshold data from Burt (2014) and other sources.
# Get species list out to csv with columns for distances.

import pandas as pd
import pyreadr
import os


base = r'C:\Users\cristianij\Documents\Projects\randomswimr\randomswimr_manuscript'
inputs = os.path.join(base, 'analysis/spatial/original_inputs')


# Read in .RData habitat data
hab_rdata = os.path.join(inputs, 'integrated_gf_maps_selected.RData')
hab = pyreadr.read_r(hab_rdata)
habdf = hab['selected_maps']
species = pd.unique(habdf.species)
species_df = pd.DataFrame(columns=['species', 'species_latin', 'min (burt)', 'max (burt)', 
                                   'avg_reported (burt)', 'category (burt)', 'notes (burt)', 
                                   'source', 'other source notes', 'movement_bin'])
species_df['species'] = species
out_csv = os.path.join(base, 'analysis/spatial/species_movement.csv')
if not os.path.exists(out_csv):
    species_df.to_csv(out_csv, index=False)


# Once filled in, make a backup in case it gets overwritten.

# There was movement information for about 34 of the 66 species. We still wanted to use all species
# in the analysis so we inferred dispersal distance for the remaining species based on either 
# movement data from other species in the same genus/family, or by inferring from extent of depth
# ranges and habitat type.

# In general, we were conservative for ones that had no information and no closely related species. 
# All rockfish without info were 10 km. Soles were given 100 km, even though some have been shown to
# move longer distances.

# For ones without distance information, unless noted, I just used fishbase for things like depth 
# ranges.

