---
title: "A random swim model to quantify MPA connectivity and movement corridors of multiple groundfish species"
author: "| John Cristiani$^{1}$^[Corresponding author: john.cristiani@dfo-mpo.gc.ca], Emily Rubidge$^{2,3}$, Mairin Deith$^4$, M. Angelica Peña$^2$,\n| Sarah Friesen$^2$, Patrick L. Thompson$^{5,6}$\n|\n|
  $^1$Pacific Biological Station, Fisheries and Oceans Canada, Nanaimo, B.C., Canada\n|
  $^2$Institute of Ocean Sciences, Fisheries and Oceans Canada, Sidney, B.C., Canada\n|
  $^3$Department of Forest and Conservation Science, University of British Columbia, \n| Vancouver, B.C., Canada\n|
  $^4$Institute for the Oceans and Fisheries, University of British Columbia, \n| Vancouver, B.C., Canada\n|
  $^5$Pacific Science Enterprise Centre, Fisheries and Oceans Canada, \n| West Vancouver, B.C., Canada\n|
  $^6$Department Zoology, University of British Columbia, Vancouver, B.C., Canada\n"
date: "`r format(Sys.time(), '%B %d, %Y')`"
output:
    bookdown::pdf_document2:
      toc: false
      number_sections: true
      fig_caption: true
      keep_tex: true
#csl: mee.csl  # citation style for journal
bibliography: library_all.bib
always_allow_html: true
link-citations: yes
header-includes:
  \usepackage[left]{lineno}
  \linenumbers
  \newcommand{\beginsupplement}{
  \setcounter{equation}{0}
  \renewcommand{\theequation}{S.\arabic{equation}}
  \setcounter{table}{0}  
  \renewcommand{\thetable}{S\arabic{table}} 
  \setcounter{figure}{0} 
  \renewcommand{\thefigure}{S\arabic{figure}}}
---
```{r, include=FALSE}
options(tinytex.verbose = TRUE)
```
```{r, echo=FALSE, include = FALSE}
library(knitcitations); cleanbib()
cite_options(citation_format = "pandoc", check.entries=FALSE)
library(bibtex)
library(knitr)
library(kableExtra)
library(tidyverse)
```

## Abstract {-}
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci. Aenean nec lorem. In porttitor. Donec laoreet nonummy augue. Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.

#### Keywords: {-}
keyword1, keyword2, keyword3
\newpage

# Introduction
Seascape connectivity results from the movement of marine organisms among spatially distinct populations or habitat patches in the ocean [@Carr2017]. These patterns of movement and connectivity have implications for population dynamics and how species can be regionally managed and conserved [@beger2022]. Given that connectivity is a design criteria of protected areas [@conventionofbiologicaldiversity2022], predicting species movement across a seascape can be useful for the assessment or design of a network of Marine Protected Areas (MPA) that are connected by the dispersal of multiple species [@DAloia2017]. However, mobility can vary greatly not just among species but also throughout the different life stages of a species [@Frisk2014; @Green2015]. This creates a significant challenge for designing and assessing MPA networks with multi-species goals.

Much marine connectivity research has focused on dispersal during the early life stages of marine organisms, such as larval dispersal in the plankton or juvenile fish moving from coastal nursery areas [@Alvarez-Noriega2020; @Brown2016; @Magris2016]. However, the dispersal of adult fish species is less studied leaving a knowledge gap in the role that adult connectivity may play in structuring populations and the ability of MPA networks to protect fish species with varying mobility [@Frisk2014; @Pittman2018]. While larval dispersal can be wide ranging for some species, it may not represent the full extent of dispersal for a species. Barriers to larval dispersal may exist due to asymmetric currents, bathymetry, and coastal topography [@Cristiani2021; @Pata2019; @Pata2022] despite some larval swimming ability which is usually limited and generally acts as a retention mechanism [@fujimura2018; @Shanks2009], thus potentially resulting in an interpretation of limited population structure. In the case of adult marine fishes with a developed swimming ability, movement is not as passive and therefore adults may not be affected by the same physical barriers potentially resulting in a larger connectivity extent [@Frisk2014]. Therefore, models predicting adult swim are a necessary component to adequately assess MPA network connectivity.

Modeling adult fish connectivity requires understanding the processes that potentially influence adult movement. For groundfish species specifically, which live on or near the bottom of the ocean, fish may move based on physical and environmental characteristics such as depth, temperature, salinity, resource availability, and substrate type and structure [@Frisk2014; @gruss2011; @saul2012]. Additionally, the current speed and direction may influence movement with the possibility of expanding or restricting movement across suitable habitat (REFs; *might be hard to find for groundfish, but there are refs for other fish like tuna and sharks*). For larvae and adults, biophysical modeling approaches that incorporate habitat suitability [@Friesen2019; @saul2012] and currents (cite Cristiani CJFAS 2024), or a combination of the two [@wilcox2023] have been used to estimate connectivity. A comprehensive model of adult swim would incorporate both habitat preferences and ocean currents. While there may be uncertainty around small scale movements such as foraging and predator avoidance behavior, at a broad spatial scale (i.e., maximum home range distance), habitat suitability models could be assumed to capture sub-scale movements. However, not every movement may be determined by external environmental properties, and therefore swim decisions could be modeled as a random process by adding a stochastic element to the model [@codling2008; @saul2012].

A model predicting adult groundfish swim spatial patterns for multiple species would allow for the assessment of MPA network connectivity. Along the coast of British Columbia, Canada, existing and proposed MPAs are being evaluated for their potential to function as an interacting network [@mpanetworkbcnorthernshelfinitiative2023; @natureunited2023]. Many of these MPAs have multi-species management objectives, however, connectivity will differ based on the varying dispersal potential of species [@cristiani2022] (switch to CJFAS ref when it is out). Therefore, quantifying movement and connectivity across multiple species with varying home ranges can identify which areas of the ocean are commonly used as movement corridors and the relevant strength of connectivity among MPAs when averaged over multiple species. Connections among MPAs can then can then reveal the role that each MPA plays in the network by acting as a population source or sink of dispersing fish - supporting population persistence across the seascape [@Fontoura2022; @Harrison2020]. This network structure that emerges from quantifying spatial swim patterns can aid managers in identifying clusters of MPAs that can be managed together as well as identifying potential gaps in protection.

Here, we predict the movement of multiple adult groundfish species along the coast of British Columbia, Canada by simulating their swim patterns with a random swim model that incorporates oceanographic currents and habitat suitability data. Using this model we aim to (1) identify areas of movement common to multiple species, (2) quantify the probability of connectivity among MPAs, and (3) quantify the potential for MPAs to act as sources and sinks of protected populations. We expect that the asymmetry of ocean currents will generate patterns of movement and connectivity that are not evident from just the patterns in the habitat suitability data (e.g., a pair of MPAs may not be connected in both directions of swim even if suitable habitat connects them). Together, these objectives provide a multispecies approach to assess how connected the MPA network is by adult fish movement, and the results could be used to identify the placement of additional MPAs that align with common corridors of movement.


# Methods

## Overview
To determine the connectivity among Marine Protected Areas and identify corridors of movement, we simulated the movement of multiple adult groundfish species using a model of random swim. To build the model, we incorporated data on MPAs (section \@ref(mpas-section)), oceanographic currents (section \@ref(currents-section)), and fish habitat suitability (section \@ref(gf-species)). We generated probabilities of movement across the seascape from the combination of the ocean currents and habitat suitability data (section \@ref(randomswimr)) and then ran simulations for each fish species (section \@ref(sims)). From the paths generated by the random swim model, we calculated the connection strengths among MPAs, the source and sink potential of each MPA, and identified areas commonly used for movement by all fish species (section \@ref(metrics)).

## Marine Protected Areas {#mpas-section}
We simulated random swims along the coast of British Columbia, Canada in the Northeast Pacific Ocean. We focused on movement originating in Marine Protected Areas in the Northern Shelf Bioregion [@Rubidge2016], which extends from Northern Vancouver Island to Southern Alaska, and from the coast of the BC mainland to the base of the coastal shelf (Figure \@ref(fig:modelcomponents)). Within this region, there are 77 established MPAs with various conservation objectives (e.g., Rockfish Conservation Areas, Wildlife Management Areas, Parks) and under different authorities (e.g., Provincial, Indigenous, Federal). We also considered an additional 51 proposed MPAs that are part of the MPA Network Action Plan to be established between 2025-2030 [@mpanetworkbcnorthernshelfinitiative2023]. We removed any MPAs located in areas where the oceanographic model data was not well resolved, such as in narrow inlets. For MPAs with spatially disparate parts, we considered these parts individually in our analysis, and for MPAs that bordered each other we combined these parts into one location for the purposes of our analysis. Lastly, some very large MPAs were split at geographically distinct locations (e.g., east and west portions of Gwaii Haanas MPA). This resulted in 47 existing and proposed MPA polygons (Figure \@ref(fig:modelcomponents)).

```{r modelcomponents, echo=FALSE, fig.pos="H", fig.align='center', fig.cap = "Data components of the random swim model. (A) The Marine Protected Area (MPA) network in the Northern Shelf Bioregion of British Columbia, Canada. MPAs in light blue were exlcluded from the anlaysis due to the low resolution of the oceanographic model in these areas. (B) The British Columbia Continental Margin (BCCM) oceanographic model. The model has a 3-km spatial resolution. (C) Habitat suitablity rasters for 66 groundfish species (lingcod data is shown here as an example)."}
knitr::include_graphics(here::here("mapping/StudyComponents.png"))
```

## Oceanographic model {#currents-section}
The oceanography currents used in the random swim model are outputs from a hindcast simulation of the British Columbia continental margin (BCCM) model [@pena2019]. This model has a horizontal resolution of 3 km on a curvilinear grid and each grid cell consists of 42 vertical levels, which differ in depth based on the seafloor depth of the cell (Figure \@ref(fig:modelcomponents)). Our analysis used  zonal (u) and meridional (v) ocean current velocities from the bottom layer of the BCCM model. These were interpolated from the curvilinear to a regular 3 km grid using a thin plate spline with the *fields* [@nychka2016] and *terra* [@hijmans2023] packages. To align the zonal and meridional ocean current velocities with the cell-centers of our model grid, which is in the BC Albers projection and North aligned, we unstaggered the values from the grid cell edges to the cell centers and then rotated the velocities from the BCCM model grid to the North and East directions. We used mean summer currents velocities (April - September) averaged across 16 years (2003-2018) because the groundfish species distribution models were based on surveys conducted only in the summer months. Thus, we cannot be sure that our estimates of habitat suitability would be valid outside of this season.

## Groundfish species {#gf-species}
The habitat suitability estimates used in the random swim model are probability of occurrence predictions from a species distribution model of groundfish in British Columbia [@thompson2023]. This model provides coastwide predictions at a 1 km resolution spanning a depth range of 10 to 1400 meters, but excluding areas where mean summer bottom salinity was below 28 psu. The model integrated presence-abscence data for 66 species (Table \@ref(tab:fish)) from three fisheries independent surveys: 1) the Fisheries and Oceans Canada groundfish synoptic bottom trawl surveys [@sinclair2003; @anderson2019], 2) the DFO groundfish hard bottom longline surveys [@lochead2006; @lochead2007; @doherty2019], and the International Pacific Halibut Commission fishery-independent setline survey [@iphc2021], that span from 2003 to 2020. The model includes covariates for log seafloor depth, bathymetric position index, substrate indices for muddiness and rockiness, log tidal speed, mean summer ocean current speed, mean summer salinity, and mean summer salinity range. The models also include year as a penalized spline and spatial and spatiotemporal Gaussian random fields that capture additional variation in species occurrence that are not associated with the environmental covariates. Predicted habitat suitability values used in this analysis is probability of occurrence, averaged across the years 2012 to 2015 (Figure \@ref(fig:modelcomponents)). See @thompson2023 for additional details about the species distribution models.

We identified the home range of each species and used this distance as a parameter in our model to limit the distance of the random swim simulations. Home range distances were binned into four general movement categories of 10, 100, 500, and 1000 kilometers following similar methodology from @Burt2014 (Table \@ref(tab:fish)). Home ranges were identified using a literature search, and for species without any information, congener species with home range information were used. We recognize that some species have very limited movement as adults (e.g., *Sebastes* spp.) and that the 10 km category may overpredict movement for an individual fish. However, as we are interested in identifying corridors of movement across suitable habitat which may happen in rare cases of dispersal or across multiple generations, this distance category allows us to identify at least minimal movement outside of an MPA.

```{r fish, echo=FALSE}
fish <- read.csv('../analysis/spatial/species_movement.csv')
fish <- fish[c('species_latin', 'species', 'movement_bin')]
kbl(fish, "latex", align="c", booktabs=TRUE, 
    caption = "Groundfish species and home range categories used to limit the random swim distances in the simulations.",
    col.names = c("Species", "Common name", "Home range category (km)"),
    linesep = '',
    position = 'h') %>%
  kable_styling(font_size=7, latex_options = 'HOLD_position') %>%
  row_spec(0, bold=TRUE)
```

## Random swim model {#randomswimr}
The model of random swim uses the oceanographic current velocities and habitat suitability data to generate probabilities of movement from one 3 km grid cell to another across the study area. At each step of the simulation the probability of moving from cell $i$ to bordering cell $j$, $p_{i,j}$ can be described as:
\begin{center}
\scalebox{1.5}{
$p_{i,j} = \frac{\frac{1}{d_{i,j}}h_j(1-v_{i,j}s)(1-u_{i,j}s)}{\sum_{k=1}^{K}\frac{1}{d_{i,k}}h_k(1-v_{i,k}s)(1-u_{i,k}s)}$
}
\end{center}
where $K$ is the number of bordering cells, $k$ is an index for those cells, $d_{i,j}$ is the euclidean distance between the centroids of the cells, $h_j$ is the suitability of the habitat for the species in patch $j$, $v_{i,j}$ is the scaled strength of the northerly direction of the current (negative values for currents moving south), $u_{i,j}$ is the scaled strength of the easterly direction of the current (negative values for currents moving west), and $s$ is a scaler parameter to weight the influence of the ocean currents (i.e., current scaler).

## Random swim simulations {#sims}
Simulations of random swim were implemented using the programming languages R and C++. We ran simulations for each of the 66 groundfish species and for two current scalers (0, 0.3) which weight the influence of the ocean currents on the movement probabilities (total of 132 simulations). For each species we initiated a swim from every MPA that had suitable habitat, as determined by a probability of occurrence greater than 0.1. Within the MPA, the swim began from the raster cell with the highest habitat suitability value. We then limited the possible destination MPAs to those within the home range distance (e.g. only MPAs within a 10 km overwater distance). From the starting cell in an MPA, a random swim simulation then proceeds by selecting an adjacent cell at random but with the selection weighted by the movement probabilities. Selection is done using the R function *sample* but implemented in C++ using the *RcppArmadillo* package. The random swim continues until a maximum number of steps is completed which varied by the species home range distance. To determine this number, we first calculated the number of steps of the straight line distance of the home range distance (e.g., 100 km of movement would require ~33 steps on a 3 km grid). We then set the maximum number of steps as 3000% of the straight line steps to allow for sufficient indirect movement from one MPA to another. Lastly, for each combination of species and current scaler value, we conducted 150 repetitions of the random swim to obtain an adequate spatial distribution of movement across the seascape. 

(*This next paragraph could go in the supplement, and then in the previous paragraph we could just say we conducted sensitivity analyses. However, I also think that with this many parameters its worth having a few extra sentences in the main text on the testing we did.*)
We conducted sensitivity analyses to determine the values for the model parameters described above: maximum steps percentage (Figure \@ref(fig:senssteps)), repetitions (Figure \@ref(fig:sensreps)), and current scaler (Figure \@ref(fig:senscs)). We tested a range of values and measured the change in the number of MPAs connected and the similarity of the swim paths for two species representing different swim potentials. The MPA connections established were divided by the total number of MPAs within the home range distance. To quantify the similarity of swim paths, for each simulation we created a raster of the grid cells used in the random swim, with each cell being a count of the times it was used in a random swim over all repetitions. We then calculated the Pearson correlation coefficient between each raster and the raster of the highest value tested (e.g., the most repetitions). To select the parameter values used in our simulations we selected the value at where the response value began to saturate. In the case of the current scaler values, connection count and rasters did not differ significantly in values along the saturation curve, and therefore we only compared current scalers of 0.3 and 0 in our analysis. A current scaler of 0 was selected so that we could compare our results to a random swim determined by just the habitat suitability values.    

## Connectivity and corridor analysis {#metrics}
From the results of the random swim simulations, we mapped the swim paths to identify corridors of probable movement. We created rasters for each combination of species and current scaler with the cell values being a count of the times a cell was used in a successful swim divided by the total number of swims. We then calculated an overall average for all species for each current scaler.

To quantify MPA connectivity we calculated the directional connection strength between each pair of MPAs as the number of successful swims between MPAs divided by the total number of repetitions. Using these connections, we then calculated network metrics of node degree for each MPA to determine how much an MPA was connected to other MPAs and in which direction as a means to estimate the relative population source and sink potential. Source potential was calculated as the weighted node out-degree (the sum of connections going out of an MPA), and sink potential was calculated as the weighted node in-degree (the sum of connections coming in to an MPA). Connectivity was calculated for each species and current scaler and also as an average across all species.

# Results
Across all species, every raster cell in the study area was used in at least one successful swim between MPAs. However, only 1.2-1.6% of raster cells were used in 10 or more successful swims (out of 150 swims) as averaged across all species (for current scalers of 0 and 0.3, respectively) (Figure \@ref(fig:gridcellfreq)). For individual species, the percent of raster cells used in at least one successful swim ranged between 4.2-100%. The percent of cells used in 10 or more successful swims ranged from 0.08-51% (Table \@ref(tab:speciesresults)).

```{r gridcellfreq, echo=FALSE, fig.pos="H", fig.align='center', fig.cap = "The frequency that each grid cell was used over 150 swims, averaged across the 66 fish species, (a) with a current scaler of 0, (b) and a current scaler of 0.3.", out.width='425px'}
knitr::include_graphics(here::here("mapping/GridCellFrequency.png"))
```

As averaged across all species, the connection strength between MPAs ranged from 0.01 to 65%. The total connections established among MPAs did not vary significantly between the two current scalers tested. Across all species, there were 1705 connections established for a current scaler of 0, and 1703 connections established for a current scaler 0.3 (Figure \@ref(fig:connections)). The locations of the strongest connections were similar between the two current scalers, however, the number of weaker connections (e.g., 10-15% connection strength) differed in Hecate Strait and Dixon Entrance. Despite more connections established with the 0 current scaler, the average connection strength was slightly higher with the 0.3 current scaler (6.09 and 6.19%, respectively). For individual species, the number of connections established ranged from 3 to 1669 (Table \@ref(tab:speciesresults)).

```{r connections, echo=FALSE, fig.pos="H", fig.align='center', fig.cap = "Connections established between MPAs with (a) a current scaler of 0 and (b) a current scaler of 0.3. Connection strength is calculated as the number of successful swims completed divided by the 150 swims per simulation. Values were averaged across all 66 species. The weakest connections between 0.01 and 10\\% are not shown for visualization purposes.", out.width='425px'}
knitr::include_graphics(here::here("mapping/Connections.png"))
```

The source and sink population potential of MPAs varied across the seascape. The weighted node out-degree, our metric of source potential, was highest for MPAs along the southern and central coast of the BC mainland. Weighted node in-degree, our metric of sink potential was highest for MPAs in Queen Charlotte Sound, Hecate Strait, and Dixon Entrance. Between the two current scalers, there were differences in source/sink potential for MPAs on the Central Coast, Dixon Entrance, and Southeast Haida Gwaii (Figure \@ref(fig:sourcesink)).

```{r sourcesink, echo=FALSE, fig.pos="H", fig.align='center', fig.cap = "The population source and sink potential for each MPA per current scaler. Source potential was quantified as the weighted out node degree, and sink potential was quantified as the weighted in node degree.", out.width='425px'}
knitr::include_graphics(here::here("mapping/SourceSink.png"))
```

# Discussion

*(discussion is still lacking in references to literature. I've mainly just written some location specific interpretation of our results for now)*

Our analysis demonstrates that swim paths can occur throughout all of the Northern Shelf Bioregion and that all MPAs can be potentially connected to at least one other MPA. However, there are areas where movement is more probable for multiple species, and some MPAs are more connected than others, thus creating differences in the source and sink population potential of MPAs. These findings suggest that the proposed arrangement of MPAs may act as an interacting network when averaged over the 66 groundfish species. The spatial distribution and dispersal potential of some species is limited, however, and therefore the conservation of certain species groups will still require considering how connectivity varies across the seascape.

We found common corridors of movement between most adjacent MPAs, and movement generally followed the coastline. There was considerable movement across Dixon entrance in the northern part of our study area, but there were no clear corridors across Hecate Strait or corridors connecting Southern Haida Gwaii to Northern Vancouver Island along the shelf (*local oceanography/connectivity lit confirming general currents/movement*). While our results do show that movement can occur in these areas, it is more diffuse. For individual species, this may be a result of low-mid habitat suitability in these areas, or habitat values that are uniform over a large area and thus generating many possible swim paths. For example, species like North Pacific Spiny Dogfish and Pacific Tomcod covered 100% of the study at least once regardless of the current scaler values. However, Spiny Dogfish covered ~45% of the study area in 10 or more swims, whereas Pacific Tomcod covered only ~18% in 10 or more swims. This may demonstrate how sharp or broad gradients in habitat suitability can create narrow or diffuse swim paths (*find some literature on habitat specialists/generalists for fish*).

The connection strength and density of connections varied across the seascape and between current scaler values. There were few strong connections across Hecate Strait. When the influence of currents were applied, connections weakened across the southern part of the Strait but additional connections were formed through the northern part of the Strait. This suggests that in areas with strong currents and without sharp gradients of habitat suitability there are more possibilities for MPA connectivity. Similarly, connections strengthened in Dixon Entrance when currents were applied. Currents in this area are stronger on average (REF) resulting in swim paths that are more narrowly directed. These differences in connection strength and density then generated differences in source/sink potential of the MPAs. Central mainland MPAs were the largest sources, with MPAs in Queen Charlotte Sound being the largest sinks. This pattern was consistent regardless of the current scaler, however, the relative values changed. When currents were applied, the East to West movement became more diffuse, and the connections to Haida Gwaii weakened. The influence of currents resulted in more uncertainty of the swim paths across Hecate Strait and thus altered source and sink potential of MPAs. Therefore, if swim paths are diffuse, then the MPAs in the center of Hecate Strait may be filling an important role as stepping stones of protected habitat to maintain connectivity of protected habitat from the mainland of BC to Haida Gwaii.

* Discussion of what this method allows us to do
  * Habitat and currents interactively shape the results
  * Effects of currents are subtle but are most evident by connection count and source/sink values.
  * Additional connections established when currents applied (even if they are weak) can still impact population dynamics and genetic diversity.
  * Rasters: More use of study area when current scaler applied. However, this didn't result in more MPAs being connected overall, suggesting that currents can make movement more diffuse. However, currents may also narrow swim paths as well making some connections stronger.

* Significance for conservation planning and assessment (gaps, multispecies management)
  * Network appears connected when averaged across species
  * No isolated areas, and at least moderate connections between most regions. However, would need to examine individual species to determine if there are any individual connectivity and conservation concerns. Our objective was to summarize and assess overall connectivity, however, underlying our results are individual species information that can be used for future analyses.
  * Areas with fewer redundant connections (i.e., both low source and sink potential) could be further examined to see if additional MPAs could create more connectivity (north/west HG, Dixon Entrance, North coast).
  * Cite the DFO/BC MPA specific literature

* Model limitations and caveats
  * many rockfish wouldn't travel this far, however, our results can represent multigenerational or rare (but genetically/demographically significant) movement. Also, it would not have been useful to model movement less than 3km since that is the cell size of our analysis, and very few MPAs are within this distance. For species with limited dispersal, the size of the MPA is the more important criteria for their protection.
  * Currents are averaged across years - there may be significant seasonal differences
  * Species-specific behavior: reaction to currents vs. preference for habitat; seasonal movements
  * We make the assumption that habitat is the primary driver of adult swim direction - we didn't compare scenarios where currents are weighted a lot higher than habitat.

* Future directions
  * combine with other connectivity modeling approaches (e.g., larval dispersal)
  * incorporate into population models (metapop and metacomm)
  * see how connectivity changes with climate change [@Friesen2021; @rubidge2024]
  * Managers can examine multi and single species results in detail to fill protection gaps
  * Species with <1km movement - adequate protection will likely be based on MPA size
  * Validation - compare to genetic connectivity

# Acknowledgements

# Data availability
Archived repo of analysis, Link to *randomswimr* package, Shiny app (hmmm, do we have the time for this? Probably not.)

\newpage
## References {-}
<div id="refs"></div>

\newpage
\beginsupplement
## Supplemental methods and results {-}

```{r senssteps, echo=FALSE, fig.pos="H", fig.align='center', fig.cap = "The sensitivity of the number of connections to the number of swim steps in a simulation. The y axis was calculated as the percentage of the total possible connections that could be established at each threshold swim distance. The x axis is a percentage of the number of steps of the straight line distance of the home range distance (e.g., 100 km of movement would require ~33 steps on a 3 km grid)", out.width="75%"}
knitr::include_graphics(here::here("analysis/spatial/outputs_20240110_sens_steps_th/outputs_processed/conns_similarityAll_slsteps_h.png"))
```

```{r sensreps, echo=FALSE, fig.pos="H", fig.align='center', fig.cap = "The sensitivity of connections established and the correlation between swim rasters to the number of repetitions per simulation. The number of connections was calculated as the percentage of the total possible connections that could be established at each threshold swim distance. To quantify the correlation of swim paths, for each simulation we created a raster of the grid cells used in the random swim, with each cell being a count of the times it was used in a random swim over all repetitions. We then calculated the Pearson correlation coefficient between each raster and the raster of the highest value tested (e.g., the most repetitions).", out.width='400px'}
knitr::include_graphics(here::here("analysis/spatial/outputs_20240111_sens_reps/outputs_processed/reps_combine.png"))
```

```{r senscs, echo=FALSE, fig.pos="H", fig.align='center', fig.cap = "The sensitivity of connections established and the correlation between swim rasters to the current scaler value. The number of connections was calculated as the percentage of the total possible connections that could be established at each threshold swim distance. To quantify the correlation of swim paths, for each simulation we created a raster of the grid cells used in the random swim, with each cell being a count of the times it was used in a random swim over all repetitions. We then calculated the Pearson correlation coefficient between each raster and the raster of the highest value tested (e.g., the largest current scaler).", out.width='400px'}
knitr::include_graphics(here::here("analysis/spatial/outputs_20240112_sens_cs/outputs_processed/cs_combine.png"))
```

```{r speciesresults, echo=FALSE, message=FALSE}
rast <- read_csv('../analysis/spatial/outputs_20240126/outputs_processed/summary_rastercells_species.csv')
conn <- read_csv('../analysis/spatial/outputs_20240126/outputs_processed/summary_connections_species.csv')
spec <- full_join(rast, conn, join_by(Species, "Current scaler"))

# join to get names properly formatted
fish <- read.csv('../analysis/spatial/species_movement.csv')
fish <- fish['species']
fish <- mutate(fish, species_lw=str_replace_all(tolower(species)," ", ''))
spec <- left_join(spec, fish, by=join_by(Species==species_lw))

spec <- spec %>% 
  mutate(Species=species) %>%        
  select(!c(...1.x, ...1.y, species, Threshold.y, "Connections mean", "Connections median")) %>%
  rename(`Threshold (km)` = Threshold.x) %>%
  filter(`Current scaler` %in% c(0, 0.3)) %>%
  arrange(Species, `Current scaler`) %>%
  relocate(`Current scaler`, .after=`Threshold (km)`)
          

kbl(spec, "latex", align="c", booktabs=TRUE, 
    caption = "Species specific results by current scaler.",
    linesep = '',
    position = 'h',
    longtable = TRUE) %>%
  kable_styling(font_size=7, position="left", latex_options = c('HOLD_position', 'repeat_header')) %>%
  row_spec(0, bold=TRUE) %>%
  column_spec(2, width="7em") %>%
  column_spec(3, width="6em") %>%
  column_spec(4, width="10em") %>%
  column_spec(5, width="10em") %>%
  column_spec(6, width="8em")
```
